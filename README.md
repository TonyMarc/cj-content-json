# CJ CONTENT JSON
Dynamic content for the static web.

## What does it do?

![Screenshot of CJ dashboard](cj-admin/resources/img/screenshot_dashboard.png)

CJ is a flattest file cms, that offers dynamic text loading through json files. The various textual contents of 
your website can managed within CJ's backend.

CJ offers a simple solution to localise static webpages on the fly and to generate content on webpages 
(html) dynamically through a predefined JSON file.

Content of html document gets replaced through iteration, this requires a span element to be present within
the element that needs to be filled with content.
E.g.

HTML filenames must not contain underscores.

```html
<h1>
	<span class="cj"> </span>
</h1>
```
CJ also offers a blog/news functionality. To enable this just add the following spans to your websites html:

```html
<h1>
	<span class="cj-headline"> </span>
</h1>
<p>
	<span class="cj-post"> </span>
</p>
<p>
	<span class="cj-date"> </span>
</p>
```
Note: Just one of those special tags needs to be present
to enable the blog/news functionality.

Those spans should not remain empty for SEO reasons, it is advisable to place your initial textual content of your default 
language inside those tags.

## Installing CJ

Download the latest release on GitLab and copy the files to the root directory of your website.<br>
CJ expects all your html and or php files to be present in that directory.
Next head over to the backend by visiting {{your domain}}/cj-admin in your browser - this will trigger the registration.

## How does CJ replace the content?	

Naming convention of JSON files:<br>
{html_filename}_{lang}.json

JavaScript (cj.js) determines filename of html file in which it is implemented and sets {html_filename}.
Cj.js checks if language is set by form, if unset, preferred browser language is used to set {lang}.
If preferred browser language can’t be obtained or no corresponding JSON file could be found, default JSON 
file is used. Default language can be set in CJ back-end.

## The content JSON File
```json
[
{"content":"foo","parent":"{html_tag}"},
{"content":"bar","parent":"{html_tag}"}
]
```



## Language selection by user

The file cj.js offers the possibility to implement a more interactive language selection.

Just add a select or a link with an onclick or onchange and pass the parameter to the javascript:

Dropdown example:

```html
<select onchange="cjLangSelect(this)">
        <option value="" disabled selected>Language</option>
        <option value="en">English</option>
        <option value="de">Deutsch</option>
</select>
```
Link example:

```html
<a href="#" onclick="cjLangSelect('en')">English</a>
```

## Recommended additions

### CSS transition

By uncommenting 
```javascript
cjClass[i].style.opacity = '1';
```
in cj.js

and including
```css
.cj{
    opacity:0;
    transition: opacity 2s;
    -webkit-transition: opacity 2s; /* Safari */
}
```
in your CSS file, the content replacement becomes a smooth fade-in transition.

### Prevent JSON Caching

The content.json files are cached by the browser, this can prevent the user from viewing your most recent changes e.g. news posts.

A .htaccess file is included.
```apacheconfig
<Files .json>
    Header unset Cache-Control
</Files>
```



