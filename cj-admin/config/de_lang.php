<?php

return [
    'languages.en' => 'Englisch',
    'languages.de' => 'Deutsch',
    'menu.settings' => 'Einstellungen',
    'menu.logout' => 'Abmelden',
    'menu.dashboard' => 'Dashboard',
    'menu.sites' => 'Seiten',
    'menu.posts' => 'Posts',
    'menu.languages' => 'Sprachen',
    'menu.users' => 'Nutzer',
    'menu.helpButton.title' => 'Hilfe',
    'dashboard.gettingStarted.headline' =>'Erste Schritte',
    'dashboard.lastModified' => 'Zuletzt geändert',
    'dashboard.latestPosts' => 'Neuste Posts',
    'contenteditor.languageVersion' => 'Sprachvariante der Seiten Inhalte',
    'contenteditor.addNew' => 'Neues Inhaltselement einfügen?',
    'contenteditor.deleteContentElement' => 'Inhaltselement löschen?',
    'contenteditor.deleteButton.title' => 'Inhaltselement löschen',
    'contenteditor.addButton.title' => 'Inhaltselement einfügen',
    'contenteditor.textarea.placeholder' => 'Text einfügen',
    'posteditor.languageVersion' => 'Sprachvariante der Posts',
    'posteditor.addNew' => 'Neuen Post einfügen?',
    'posteditor.deletePost' => 'Post löschen?',
    'posteditor.deleteButton.title' => 'Post löschen',
    'posteditor.addButton.title' => 'Post hinzufügen',
    'languageeditor.setLanguageVersion' => 'Sprachvarianten verwalten:',
    'languageeditor.active' => 'Aktiv',
    'languageeditor.default' => 'Standard',
    'languageeditor.delete' => 'Löschen',
    'languageeditor.addLanguage' => 'Sprache hinzufügen',
    'languageeditor.languagePlaceholder' => 'Sprache',
    'languageeditor.abbreviationPlaceholder' => 'Kürzel',
    'languageeditor.languageTitle' => 'Geben Sie einen Namen für die neue Sprache ein.',
    'languageeditor.abbreviationTitle' => 'Das Kürzel muss aus zwei Buchstaben bestehen.',
    'users.deleteUsers' => 'Nutzer löschen?',
    'users.changePassword' => 'Passwort ändern für ',
    'users.user' => 'Nutzer',
    'users.role' => 'Rolle',
    'users.actions' => 'Aktionen',
    'users.button.deleteUser' => 'Nutzer löschen',
    'users.button.changePassword' => 'Passwort ändern',
    'users.addUser' => 'Nutzer hinzufügen',
    'users.language' => 'Sprache',
    'users.createRegistrationLink' => 'Registrierungslink erzeugen',
    'users.registrationLink' => 'Registrierungslink',
    'users.userRole.title' => 'Rolle wählen',
    'users.preferredLanguage.title' => 'Bevorzugte Sprache der Benutzeroberfläche wählen',
    'users.button.deleteRegistrationLink.title' => 'Registrierungslink löschen',
    'users.deleteRegistrationLink' => 'Registrierungslink löschen?',
    'users.copyConfirmation' => 'Link wurde in die Zwischenablage kopiert.',
    'users.button.copyLink.title' => 'Registrierungslink in die Zwischenablage kopieren',
    'users.domain.title' => 'Domain ohne /cjadmin/',
    'users.button.activateUser' => 'Nutzer aktivieren',
    'users.activateUser' => 'Login aktivieren für ',
    'users.button.changeUserRole' => 'Rolle ändern',
    'users.changeUserRole' => 'Rolle ändern für ',
    'settings.customLogo' => 'Logo des Anmeldebildschirms',
    'settings.customLogo.fileInfo' => 'Quadratische PNG Datei',
    'settings.customLogo.uploadImage' => 'Bild hochladen',
    'setting.languageSetting' => 'Sprache der Benutzeroberfläche',
    'settings.changePassword' => 'Passwort ändern',
    'button.reset' => 'Reset',
    'button.save' => 'Speichern',
    'button.yes' => 'Ja',
    'button.no' => 'Nein',
    'button.delete' => 'Löschen',
    'button.register' => 'Register',
    'button.createRegistrationLink' => 'Link erstellen',
    'button.ok' => 'OK',
    'form.username.placeholder' => 'Nutzername',
    'form.username.title' => 'Mindestlänge drei Zeichen, keine Leerzeichen, kein Doppelpunkt (:)',
    'form.password.placeholder' => 'Passwort',
    'form.password.title' => 'Derzeitiges Passwort',
    'form.newPassword.placeholder' => 'Neues Passwort',
    'form.newPassword.title' => 'Mindestlänge acht Zeichen inklusive einer Zahl/Buchstabe',
    'form.repeatPassword.placeholder' => 'Passwort wiederholen',
    'form.repeatPassword.title' => 'Passwort wiederholen',
    'help.dashboard.admin' => '<ul><li>wählen Sie <i>Sprachen</i>, um die verfügbaren Sprachvarianten Ihrer Webseite zu verwalten und die Standard Sprache festzulegen</li><li>öffnen Sie den Kontenteditor durch die Auswahl von <i>Seiten</i> und dem Dateinamen der Webseite, deren Inhalte Sie bearbeiten möchten</li><li>wählen Sie <i>Posts</i> und den Dateinamen, um Beiträge zu bearbeiten (erfordert mindestens ein &lt;span&gt; Element der cj-headline, cj-post oder cj-date class)</li><li>verwalten Sie Nutzerkonten durch die Auswahl von <i>Nutzer</i></li><li>Änderungen der Benutzeroberfläche oder Ihres Passworts können Sie mit der Auswahl von <i>Einstellungen</i> vornehmen</li></ul>',
    'help.dashboard.chiefEditor' => '<ul><li>wählen Sie <i>Sprachen</i>, um die verfügbaren Sprachvarianten Ihrer Webseite zu verwalten und die Standard Sprache festzulegen</li><li>öffnen Sie den Kontenteditor durch die Auswahl von <i>Seiten</i> und dem Dateinamen der Webseite, deren Inhalte Sie bearbeiten möchten</li><li>wählen Sie <i>Posts</i> und den Dateinamen, um Beiträge zu bearbeiten (erfordert mindestens ein &lt;span&gt; Element der cj-headline, cj-post oder cj-date class)</li><li>Änderungen der Benutzeroberfläche oder Ihres Passworts können Sie mit der Auswahl von <i>Einstellungen</i> vornehmen</li></ul>',
    'help.dashboard.editor' => '<ul><li>wählen Sie <i>Posts</i> und den Dateinamen, um Beiträge zu bearbeiten (erfordert mindestens ein &lt;span&gt; Element der cj-headline, cj-post oder cj-date class)</li><li>Änderungen der Benutzeroberfläche oder Ihres Passworts können Sie mit der Auswahl von <i>Einstellungen</i> vornehmen</li></ul>',
    'help.contenteditor' => '<p>Ihre Webseite wird mit dem ersten Aufruf dieses Moduls verarbeitet. Span Elemente der cj class werden analysiert, deren Inhalt wird in eine JSON Datei gespeichert.</p>

            <p>Sie können diese Inhalte mit dem Editor bearbeiten. Beim Aufruf Ihrer Webseite werden diese Inhalte aus der JSON Datei ausgelesen und in der entsprechenden Sprachvariante angezeigt (wenn Sie mehrere Sprachversionen verwenden).</p>

            <p>Für den Fall, dass Sie Ihre Webseite bearbeiten (hinzufügen/entfernen von &lt;span&gt; Elementen der cj class), müssen die JSON Dateien ebenfalls angepasst werden. Fügen Sie einfach neue Inhalte hinzu <img class="helpIcons" src="../resources/img/addButton_274156.svg"> oder löschen Sie überflüssige Inhaltselemente <img class="helpIcons" src="../resources/img/delete_274156.svg">.</p>',
    'help.posteditor' => '<p>Ihre Webseite wird mit dem ersten Aufruf dieses Moduls verarbeitet. Span Elemente der cj-headline, cj-post und cj-date class werden analysiert, deren Inhalt wird in eine JSON Datei gespeichert.

            <p>Sie können diese Inhalte mit dem Editor bearbeiten. Beim Aufruf Ihrer Webseite werden die Inhalte der Posts aus der JSON Datei ausgelesen und in der entsprechenden Sprachvariante angezeigt (wenn Sie mehrere Sprachversionen verwenden).</p>

            <p>Sie können neue Posts hinzufügen <img class="helpIcons" src="../resources/img/addButton_274156.svg"> oder überflüssige löschen <img class="helpIcons" src="../resources/img/delete_274156.svg">. Die Anzahl der Posts, welche auf Ihrer Webseite ausgegeben werden hängt von der Anzahl der &lt;span&gt; Elemente mit der cj-headline, cj-post und cj-date class ab.</p>',
    'help.languageeditor' => '<p>Markieren Sie die Checkbox &#9745; neben <i>Aktiv</i>, um eine Sprachvariante zu aktivieren. Eine aktivierte Sprachvariante kann über dem Editor im Seiten- und Post-Modul ausgewählt werden. Was die Bearbeitung der dazugehörigen Inhalte erlaubt.</p>

            <p>Wenn Sie eine Sprache deaktivieren, können die Inhalte dieser Sprachvariante noch im Editor bearbeitet werden. Die Inhalte dieser Sprache werden allerdings nicht mehr auf Ihrer Webseite ausgegeben. Durch das Löschen einer Sprache werden alle JSON Dateien dieser Sprache gelöscht.</p>

            <p>Es kann nur eine Standard Sprache gewählt werden. Diese Sprache dient als Ausweichlösung, welche angezeigt wird, wenn die bevorzugte Browsersprache eines Webseiten Besuchers nicht mit Ihren Sprachvarianten übereinstimmt.</p>

            <p>Sie können Ihre eigenen Sprachen hinzufügen. Geben Sie lediglich einen Namen, sowie ein Kürzel an und wählen Sie <img class="helpIcons" src="../resources/img/addButton_274156.svg"> und \'Speichern\' im Anschluss. Gültige Kürzel entnehmen Sie dem <i>ISO 639-1</i> Abkürzungsverzeichnis.</p>',
    'help.users' => '<p>Es existieren drei verschiedene Rollen für Nutzer:</p>
            <ul>
                <li>Admin: kann Sprachen verwalten, Seiten/Posts editieren, Nutzer verwalten
                    und erweiterte Einstellungen vornehmen</li>
                <li>Chief Editor: kann Sprachen verwalten und Seiten/Posts editieren</li>
                <li>Editor: kann Posts editieren</li>
            </ul>
            <p>Das Nutzermodul bietet unterschiedliche Aktionen:</p>
            <ul>
                <li><img class="helpIcons" src="../resources/img/changePassword.svg"> Passwort eines Nutzers ändern</li>
                <li><img class="helpIcons" src="../resources/img/delete_274156.svg"> Nutzer löschen</li>
                <li><img class="helpIcons" src="../resources/img/changeUserRole.svg"> Rolle eines Nutzers ändern</li>
                <li><img class="helpIcons" src="../resources/img/addButton_274156.svg"> Nutzer aktivieren</li>
            </ul>
            <p>Neue Nutzer können direkt über das Backend oder Registrierungslinks hinzugefügt werden. Sollte die Domain, welche unter <i>Registrierungslink erzeugen</i> angezeigt wird, nicht Ihrer eigenen Domain entsprechen, können Sie diese bearbeiten. Registrierungslinks werden nach ihrer Benutzung gelöscht. Die neuen Benutzer müssen anschließend im Nutzermodul aktiviert werden.</p>
            <p>Registrierungslinks können mittels <img class="helpIcons" src="../resources/img/copyLink.svg"> in die Zwischenablage kopiert und mittels <img class="helpIcons" src="../resources/img/delete_274156.svg"> gelöscht werden.</p>',
    'help.settings.admin' => '<p>Richten Sie sich ein benutzerdefiniertes Logo für den Anmeldebildschirm ein. Ihr Bild <i>muss</i> als quadratische PNG Datei vorliegen und sollte eine Größe von 2MB nicht überschreiten.</p>

            <p>Sie können die Sprache der Benutzeroberfläche anpassen. Nach der Sprachanpassung sollten Sie sich ab- und wieder anmelden, um die Änderungen sehen zu können.</p>',
];
