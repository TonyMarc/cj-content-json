<?php
/**
 * Version and global paths
 */
return [
    'version'         => '2.2.0',
    'translations'    => ['en','de'],
    'baseAdminPath'   => '/cj-admin/',
    'baseContentPath' => '/cj-content/',
    'controllerPath'  => '/cj-admin/controller/',
    'modelPath'       => '/cj-admin/model/',
    'viewPath'        => '/cj-admin/view/',
    'servicePath'     => '/cj-admin/service/',
    'sitesContent'    => '/cj-content/sites/',
    'postsContent'    => '/cj-content/posts/',
    'languagePath'    => '/cj-content/languages/',
    'cssPath'         => '/cj-admin/resources/css/',
    'analyticsPath'   => '../../cj-content/analytics/',
];
