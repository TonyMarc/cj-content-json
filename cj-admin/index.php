<?php
// is true if a user is logged in
$isUserLoggedIn = isset($_SESSION['username']); //TODO check if it exists

// holds the name of the user
// TODO check session validity
// redirect to the admin interface if the user is currently logged in.
if ($isUserLoggedIn) {
    header('Location: view/backend.php');
    exit;

// redirect to login if user is not logged in
} else {
    header('Location: view/login.php');
    exit;
}
