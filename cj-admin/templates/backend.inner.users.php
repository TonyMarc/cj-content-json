<?php
include_once('../controller/ValidationController.php');
include_once('../controller/RegistrationController.php');
include_once('../controller/UserController.php');
include_once('../service/RegisterService.php');

use cj\controller\ValidationController;
use cj\controller\RegistrationController;
use cj\controller\UserController;
use cj\service\RegisterService;

// Include translation config
$translation = include('../config/' . $_SESSION['lang'] . '_lang.php');

if ($_SESSION['type'] < 2) {
    $validation = new ValidationController();
    $registration = new RegistrationController();
    $usercontroller = new UserController();
    $register = new RegisterService();

    if (isset($_POST['register'])) {
        $type = $_POST['type'];
        $lang = $_POST['lang'];
        if (isset($_POST['username'])) {
            if ($validation->comparePasswords($_POST['password'], $_POST['passwordRepeat'])) {
                if ($registration->createUser($_POST['username'], $validation->createHash($_POST['password']), $type,
                    $lang, '1')) {
                    header('Location: backend.php?view=users');
                    exit;
                }
            }
        }
    }
    if (isset($_POST['delete'])) {
        $username = $_POST['delete'];
        if ($registration->deleteUser($username)) {
            header('Location: backend.php?view=users');
            exit;
        }
    }
    if (isset($_POST['changePassword'])) {
        if ($validation->comparePasswords($_POST['newPassword'], $_POST['newPasswordRepeat'])) {
            $usercontroller->changePassword($_POST['changePassword'], $validation->createHash($_POST['newPassword']));
            header('Location: backend.php?view=users');
            exit;
        }
    }
    if (isset($_POST['registrationLink'])) {
        RegisterService::createLinkHash($_POST['userRoleLink'], $_POST['backendLanguageLink']);
        RegisterService::createDomainConfig($_POST['domain']);
        header('Location: backend.php?view=users#links');
        exit;
    }
    if (isset($_POST['deleteRegistrationLink'])){
        $register->deleteHash($_POST['deleteRegistrationLink']);
        header('Location: backend.php?view=users#links');
        exit;
    }
    if (isset($_POST['activateUser'])) {
        $usercontroller->changeTag($_POST['activateUser'],'1');
        header('Location: backend.php?view=users');
        exit;
    }
    if (isset($_POST['changeUserRole'])) {
        $usercontroller->changeType($_POST['changeUserRole'], $_POST['userRoleChange']);
        header('Location: backend.php?view=users');
        exit;
    }
} else {
    header('Location: backend.php?');
    exit;
}
?>
<h2><?php echo $translation['menu.users']; ?></h2>
<!-- Modals -->
<div id="modalDeleteDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeDeleteDialog">&times;</span>
        <h3><?php echo $translation['users.deleteUsers']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="deleteNo"><?php echo $translation['button.no']; ?></button>
            <form name="delete" action="" class="form-user-deletion" method="post">
                <button type="submit"
                        name="delete"
                        id="deletion-login"
                        value=""><?php echo $translation['button.delete']; ?></button>
            </form>
        </div>
    </div>
</div>

<div id="modalChangePasswordDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeChangePasswordDialog">&times;</span>
        <h3 id="headlineChangePasswordDialog"><?php echo $translation['users.changePassword'] ?></h3>
        <div class="buttonsModal">
            <form name="changePassword"
                  action=""
                  class="form-changePassword"
                  method="post"
                  autocomplete="off">
                <div class="formGroup">
                    <?php
                    // Output error messages
                    echo '<p class="errorMessage">' . $usercontroller->getErrors() . '</p>';
                    echo '<p class="errorMessage">' . $validation->getErrors() . '</p>';
                    $usercontroller->errorMessage = "";
                    ?>
                    <input type="password"
                           placeholder="<?php echo $translation['form.newPassword.placeholder']; ?>"
                           name="newPassword"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.newPassword.title']; ?>"
                           id="inputNewPw"
                           autocomplete="off">
                    <input type="password"
                           placeholder="<?php echo $translation['form.repeatPassword.placeholder']; ?>"
                           name="newPasswordRepeat"
                           required="required"
                           pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                           title="<?php echo $translation['form.repeatPassword.title']; ?>"
                           autocomplete="off"
                           onfocus="comparePasswords(this)">
                    <button name="changePassword"
                            type="submit"
                            id="changePassword"
                            value=""><?php echo $translation['button.save']; ?></button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="modalActivateUser" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeActivateUser">&times;</span>
        <h3 id="headlineActivateUserDialog"><?php echo $translation['users.activateUser']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="activateNo"><?php echo $translation['button.no']; ?></button>
            <form name="activateUser" action="" class="form-user-deletion" method="post">
                <button type="submit"
                        name="activateUser"
                        id="activateUser"
                        value=""><?php echo $translation['button.yes']; ?></button>
            </form>
        </div>
    </div>
</div>

<div id="modalChangeUserRole" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeChangeUserRole">&times;</span>
        <h3 id="headlineChangeUserRoleDialog"><?php echo $translation['users.changeUserRole']; ?></h3>
        <div class="buttonsModal">
            <form name="changeUserRole" action="" class="form-user-deletion" method="post">
                <div class="formGroup">
                <div class="is-admin">
                    <select name="userRoleChange"
                            required="required"
                            id="userRoleChange"
                            title="<?php echo $translation['users.userRole.title']; ?>">
                        <option disabled selected value><?php echo $translation['users.role'] ?></option>
                        <option value="1">Admin</option>
                        <option value="0">Chief Editor</option>
                        <option value="2">Editor</option>
                    </select>
                </div>
                <button type="submit"
                        name="changeUserRole"
                        id="changeUserRoleYes"
                        value=""><?php echo $translation['button.save']; ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modalDeleteRegistrationLink" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeDeleteRegistrationLink">&times;</span>
        <h3><?php echo $translation['users.deleteRegistrationLink']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="deleteRegistrationLinkNo"><?php echo $translation['button.no']; ?></button>
            <form name="delete" action="" class="form-user-deletion" method="post">
                <button type="submit"
                        name="deleteRegistrationLink"
                        id="deleteRegistrationLink"
                        value=""><?php echo $translation['button.delete']; ?></button>
            </form>
        </div>
    </div>
</div>

<div id="modalCopyConfirmation" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeCopyConfirmation">&times;</span>
        <h3><?php echo $translation['users.copyConfirmation']; ?></h3>
        <input id="copyConfirmationInput" type="text">
        <div class="buttonsModal">
            <button type="button"
                    id="copyConfirmationOK"><?php echo $translation['button.ok']; ?></button>
        </div>
    </div>
</div>

<section class="user-area">
    <table class="userTable" cellspacing="0">
        <tr>
            <th><h3><?php echo $translation['users.user'] ?></h3></th>
            <th><h3><?php echo $translation['users.role'] ?></h3></th>
            <th><h3><?php echo $translation['users.actions'] ?></h3></th>
        </tr>
        <?php
        foreach ($this->_['users'] as $user) {
            ?>
            <tr>
                <td>
                    <?= $user['username'] ?>
                </td>

                <td>
                    <?php
                    if ($user['type'] == 1) {
                        echo 'Admin';
                    }
                    if ($user['type'] == 0) {
                        echo 'Chief Editor';
                    }
                    if ($user['type'] == 2) {
                        echo 'Editor';
                    } ?>
                </td>
                <td>
                    <?php
                    if ($user['username'] != $_SESSION['username']) {
                        echo '<button class="deleteUser" 
                        value="' . $user['username'] . '" onclick="showDeleteModal(this)" title="' . $translation['users.button.deleteUser'] . '">X</button>';
                        if ($validation->isUserActive($user['username'])) {
                            echo '<button class="changePassword" value="' . $user['username'] . '" onclick="showChangePasswordModal(this)" title="' . $translation['users.button.changePassword'] . '">C</button>';
                            echo '<button class="changeUserRole" value="' . $user['username'] . '" onclick="showChangeUserRoleModal(this)" title="' . $translation['users.button.changeUserRole'] . '">U</button>';
                        }
                        if (!$validation->isUserActive($user['username'])){
                            echo '<button class="activateUser" value="' . $user['username'] .'" onclick="showUserActivationModal(this)" title="' . $translation['users.button.activateUser'] . '">A</button>';
                        }
                    } ?>
                </td>


            </tr>
            <?php
        } ?>
    </table>

    <div>
        <div class="card">
            <div class="cardMainUsers">
                <label><h3><?php echo $translation['users.addUser']; ?></h3></label>
            </div>
            <div class="cardFooter">
                <form action="" class="form-registration" method="post" autocomplete="off">
                    <?php
                    // Output of error messages
                    echo $registration->getErrors() . "\n";
                    echo $validation->getErrors();
                    $registration->errorMessage = "";
                    $validation->errorMessage = "";
                    ?>

                    <div class="add-user">
                        <div class="formGroupAddUser">
                            <input type="text"
                                   placeholder="<?php echo $translation['form.username.placeholder']; ?>"
                                   name="username"
                                   required="required"
                                   pattern="[^:\s]{3,}"
                                   title="<?php echo $translation['form.username.title']; ?>"
                                   autocomplete="off">
                            <input type="password"
                                   id="inputPw"
                                   placeholder="<?php echo $translation['form.password.placeholder']; ?>"
                                   name="password"
                                   required="required"
                                   pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                                   title="<?php echo $translation['form.newPassword.title']; ?>"
                                   autocomplete="off">
                            <input type="password"
                                   placeholder="<?php echo $translation['form.repeatPassword.placeholder']; ?>"
                                   name="passwordRepeat"
                                   required="required"
                                   pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                                   title="<?php echo $translation['form.repeatPassword.title']; ?>"
                                   onfocus="compareNewUserPasswords(this)"
                                   autocomplete="off"
                                   id="inputPwRepeat">


                            <div class="is-admin">
                                <select name="type"
                                        required="required"
                                        id="user-role"
                                        title="<?php echo $translation['users.userRole.title']; ?>">
                                    <option disabled selected value><?php echo $translation['users.role'] ?></option>
                                    <option value="1">Admin</option>
                                    <option value="0">Chief Editor</option>
                                    <option value="2">Editor</option>
                                </select>
                            </div>

                            <div class="backendLanguage">
                                <select name="lang"
                                        required="required"
                                        id="backendLanguage"
                                        title="<?php echo $translation['users.preferredLanguage.title']; ?>">
                                    <option disabled selected
                                            value><?php echo $translation['users.language'] ?></option>
                                    <?php
                                    $config = include('../config/config.php');
                                    foreach ($config['translations'] as $lang) {
                                        echo '<option value="' . $lang . '"';
                                        $translationKey = 'languages.' . $lang;
                                        echo '>' . $translation[$translationKey] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="register">
                                <button type="submit"
                                        name="register"
                                        id="registration-login"
                                        value="update"><?php echo $translation['button.register']; ?></button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

    <div>
    <div class="card">
        <div class="cardMainUsers">
            <label><h3><?php echo $translation['users.createRegistrationLink']; ?></h3></label>
        </div>
        <div id="cardFooterRegistrationLink" class="cardFooter">
            <form action="" name="registrationLink" class="form-registration" method="post" autocomplete="off">
                <div class="add-user-link">
                    <div class="formGroupAddUser">
                        <div>
                            <input class="domainLabel" value="Domain:" disabled>
                        </div>
                        <input type="text"
                               id="domainInput"
                               name="domain"
                               required="required"
                               title="<?php echo $translation['users.domain.title']; ?>"
                               autocomplete="off"
                               value="<?php
                               if (RegisterService::getDomainConfig()){
                                   echo RegisterService::getDomainConfig();
                               } else {
                                   echo RegisterService::getDomainName();
                               }?>">

                        <div class="is-admin">
                            <select name="userRoleLink"
                                    required="required"
                                    id="userRoleLink"
                                    title="<?php echo $translation['users.userRole.title']; ?>">
                                <option disabled selected value><?php echo $translation['users.role'] ?></option>
                                <option value="1">Admin</option>
                                <option value="0">Chief Editor</option>
                                <option value="2">Editor</option>
                            </select>
                        </div>
                        <div class="backendLanguage">
                            <select name="backendLanguageLink"
                                    required="required"
                                    id="backendLanguageLink"
                                    title="<?php echo $translation['users.preferredLanguage.title']; ?>">
                                <option disabled selected value><?php echo $translation['users.language'] ?></option>
                                <?php
                                $config = include('../config/config.php');
                                foreach ($config['translations'] as $lang) {
                                    echo '<option value="' . $lang . '"';
                                    $translationKey = 'languages.' . $lang;
                                    echo '>' . $translation[$translationKey] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="register">
                            <button type="submit"
                                    name="registrationLink"
                                    id="registrationLink"
                                    value="update"><?php echo $translation['button.createRegistrationLink']; ?></button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    </div>

    <?php
    if ($register->getLinkHashes()) {
    ?>
        <table class="userTable" cellspacing="0">
            <tr>
                <th><h3><?php echo $translation['users.registrationLink'] ?></h3><a name="links"></a></th>
                <th><h3><?php echo $translation['users.actions'] ?></h3></th>
            </tr>
            <?php
            foreach ($register->getCredentials() as $linkHash) {
            ?>
            <tr>
                <td>
                    <textarea title="<?php echo $translation['users.registrationLink']; ?>" disabled="disabled"><?php echo RegisterService::createRegistrationLink(RegisterService::getDomainConfig(), $linkHash['link']);?></textarea>
                </td>
                <td>
                    <button class="copyLink" value="<?php echo RegisterService::createRegistrationLink(RegisterService::getDomainConfig(), $linkHash['link']);?>"
                            onclick="showCopyConfirmationModal(this)"
                            title="<?php echo $translation['users.button.copyLink.title']; ?>">C</button>
                    <button class="deleteUser"
                            value="<?php echo $linkHash['link']; ?>" onclick="showRegistrationLinkDeleteModal(this)" title="<?php echo $translation['users.button.deleteRegistrationLink.title']; ?>">X</button>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    <?php
    }
    ?>


</section>

<script src="../resources/js/users.js"></script>
<script src="../resources/js/pattern.js"></script>

