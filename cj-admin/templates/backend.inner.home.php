<?php
include_once('../service/FileHandler.php');
include_once('../service/JsonService.php');

use cj\service\FileHandler;
use cj\service\JsonService;

// Include translation config
$translation = include('../config/' . $_SESSION['lang'] . '_lang.php');

$fileHandler = new FileHandler();
$config = include('../config/config.php');
?>

<h2><?php echo $translation['menu.dashboard']; ?></h2>

<?php
if (empty($fileHandler->getContentOrPostFiles($config['sitesContent'])) && empty($fileHandler->getContentOrPostFiles($config['postsContent']))){

?>
    <div class="card">
        <div class="cardMain"><label><h3><?php echo $translation['dashboard.gettingStarted.headline']; ?></h3></label></div>
        <div class="cardFooterLatestPost gettingStarted">
            <?php
            if ($_SESSION['type'] == 1) {
                echo $translation['help.dashboard.admin'];
            }
            if ($_SESSION['type'] == 0) {
                echo  $translation['help.dashboard.chiefEditor'];
            }
            if ($_SESSION['type'] == 2) {
                echo $translation['help.dashboard.editor'];
            }
            ?>
        </div>
    </div>
<?php
}
?>

<?php
if (!empty($fileHandler->getContentOrPostFiles($config['sitesContent']))) {
    $newestContentFiles = $fileHandler->getNewestFiles($config['sitesContent']);
    $newestPostFiles = $fileHandler->getNewestFiles($config['postsContent']);
    ?>

    <div class="card">
        <div class="cardMain">
            <label><h3><?php echo $translation['dashboard.lastModified'] ?></h3></label>
        </div>
        <div class="cardFooterLatestPost">
            <div class="grid">
                <?php if ($_SESSION['type'] < 2) {
                    ?>
                    <div class="lastModifiedSites">
                        <div class="lastModifiedHeadline"><?php echo $translation['menu.sites'] ?></div>

                        <?php
                        $i = 0;
                        foreach ($newestContentFiles as $file) {
                            $fileName = explode('.', $file);
                            $fileComponents = explode('_', $fileName[0]);
                            $linkSuffix = $fileComponents[0] . '&lang=' . $fileComponents[1];
                            ?>
                            <div class="lastModified">
                                <a href=<?= "../view/backend.php?view=contenteditor&site=$linkSuffix" ?>>
                                    <?php
                                    echo $fileHandler->truncate($file, 20);
                                    ?>
                                </a>
                            </div>

                            <?php
                            $i++;
                            if ($i == 3) {
                                break;
                            }
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="lastModifiedPosts">
                    <div class="lastModifiedHeadline"><?php echo $translation['menu.posts'] ?></div>

                    <?php
                    $i = 0;
                    foreach ($newestPostFiles as $file) {
                        $fileName = explode('.', $file);
                        $fileComponents = explode('_', $fileName[0]);
                        $linkSuffix = $fileComponents[0] . '&lang=' . $fileComponents[1];
                        ?>
                        <div class="lastModified">
                            <a href=<?= "../view/backend.php?view=posteditor&site=$linkSuffix" ?>>
                                <?php
                                echo $fileHandler->truncate($file, 20);
                                ?>
                            </a>
                        </div>

                        <?php
                        $i++;
                        if ($i == 3) {
                            break;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

?>

<?php
if (!empty($fileHandler->getContentOrPostFiles($config['postsContent']))) {
    $newestPosts = $fileHandler->getNewestFiles($config['postsContent']);
    $j = 0;
    ?>
    <div class="card">
        <div class="cardMain">
            <label><h3><?php echo $translation['dashboard.latestPosts'] ?></h3></label>
        </div>
        <div class="cardFooterLatestPost">
            <?php
            foreach ($newestPosts as $newestPost) {
                $postData = $fileHandler->getFileAndLang($newestPost);
                $post = JsonService::loadPostJsonFile($postData['file'], $postData['lang']);
                ?>
                <?php
                $linkSuffix = $postData['file'] . '&lang=' . $postData['lang'];

                if (!empty($post[0]['headline'])) {
                    ?>
                    <div class="headlineLatestPost">
                        <a href=<?= "../view/backend.php?view=posteditor&site=$linkSuffix" ?>>
                            <?php
                            echo $fileHandler->truncate($post[0]['headline'], 100) . ' ';
                            if (!empty($post[0]['date'])) {
                                echo '(' . $post[0]['date'] . ')';
                            }
                            ?>
                        </a>
                    </div>
                    <?php
                }
                if (!empty($post[0]['post'])) {
                    echo '<div class="postLatestPost">' . $fileHandler->truncate($post[0]['post'], 300) . '</div>';
                }
                ?>
                <?php
                ?>
                <div class="linkLatestPost">
                    <a href=<?= "../view/backend.php?view=posteditor&site=$linkSuffix" ?>>
                        <?php echo $postData['file'] . '_' . $postData['lang'] ?>
                    </a>
                </div>
                <?php
                $j++;
                if ($j == 3) {
                    break;
                }
                if (count($newestPosts) > 1 && count($newestPosts) != $j) {
                    echo '<hr>';
                }

            }


            ?>
        </div>
    </div>
    <?php
}





