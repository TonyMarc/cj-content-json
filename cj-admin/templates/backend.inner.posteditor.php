<?php

use cj\service\JsonService;

// Include translation config
$translation = include ('../config/'. $_SESSION['lang'] .'_lang.php');
?>
<div>
    <h2><?= $this->_['site'] ?></h2>
    <form class="language-select-form" action="" method="get">
        <?php
        // we want to keep the get parameters when submitting the form
        foreach ($_GET as $name => $value) {
            $name = htmlspecialchars($name);
            $value = htmlspecialchars($value);
            if ($name != 'lang') {
                echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
            }
        }
        ?>
        <select name="lang"
                size="1"
                onchange="this.form.submit()"
                title="<?php echo $translation['posteditor.languageVersion']; ?>">
            <?php
            $selectedLang = isset($_GET['lang']) ? $_GET['lang'] : JsonService::getDefaultLanguage();
            foreach ($this->_['languages'] as $language) {
                $translationKey = 'languages.' . $language['short'];
                ?>
                <option value="<?= $language['short'] ?>"
                    <?php echo $language['short'] == $selectedLang ? "selected" : "" ?>><?php if ($translation[$translationKey] != '') {
                        echo $translation[$translationKey];
                    } else {
                        echo $language['long'];
                    }
                    ?></option>
                <?php
            } ?>
        </select>
    </form>
</div>

<!-- Modal dialog -->
<div id="modalAddDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeAddDialog">&times;</span>
        <h3><?php echo $translation['posteditor.addNew']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="addNo"><?php echo $translation['button.no']; ?></button>
            <button type="button"
                    id="addYes"><?php echo $translation['button.yes']; ?></button>
        </div>
    </div>
</div>
<div id="modalDeleteDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeDeleteDialog">&times;</span>
        <h3><?php echo $translation['posteditor.deletePost']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="deleteNo"><?php echo $translation['button.no']; ?></button>
            <button type="button"
                    id="deleteYes"><?php echo $translation['button.yes']; ?></button>
        </div>
    </div>
</div>

<form id="post-editor" action="" method="post">
    <?php
    $i = 0;
    foreach ($this->_['post'] as $content) {
        if (isset($content['updated'])) {
            break;
        } ?>
        <div class="postGroup">
            <div class="card">
                <img src="../resources/img/delete.svg"
                     id="deleteButton"
                     title="<?php echo $translation['posteditor.deleteButton.title']; ?>"
                     class="remove"
                     onclick="showDeleteModal(this)">
                <img src="../resources/img/addButton.svg"
                     id="addButton"
                     class="add"
                     title="<?php echo $translation['posteditor.addButton.title']; ?>"
                     onclick="showAddModal(this)">
                <!-- Code must not be reformated! -->
                <div class="cardMain">
                    <label class="headline"><h3>headline</h3></label>
                </div>
                <div class="cardFooterPosts"><textarea rows="2" placeholder="Write your text here!" name="modified_headline[]"><?= $content['headline'] ?></textarea></div>
                <div class="cardMain">
                    <label><h3>post</h3></label>
                </div>
                <div class="cardFooterPosts"><textarea rows="5" placeholder="Write your text here!" name="modified_post[]"><?= $content['post'] ?></textarea></div>
                <div class="cardMain">
                    <label><h3>date</h3></label>
                </div>
                <div class="cardFooter"><textarea rows="2" placeholder="Write your text here!" name="modified_date[]"><?= $content['date'] ?></textarea></div>
                <!-- Code must not be reformated! -->
            </div>
            <?php $i++; ?>
            <?php if (count($this->_['post']) -1 > $i) {
                echo '<hr>';
            }
            ?>
        </div>
        <?php
    } ?>

    <div class="save">
        <button type="submit"><?php echo $translation['button.save']?></button>
    </div>

</form>

<script src="../resources/js/posteditor.js"></script>
