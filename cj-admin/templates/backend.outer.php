<?php
include_once('../service/FileHandler.php');

use cj\service\FileHandler;

// Include translation config
$translation = include ('../config/'. $_SESSION['lang'] .'_lang.php');

$config = include ('../config/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="../resources/css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CJ Admin</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resources/img/favicon.ico">
    <link rel="stylesheet" href="<?php echo $this->_['backend_custom_css']; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<!-- Help modal -->
<?php include_once ('../partials/backend.outer.help.php')?>
<!-- Click-Overlay -->
<div id="overlay" class="overlay"></div>

<header>
    <a href="javascript:void(0);" id="menuButton" class="icon" onclick="menu()">&#9776;</a>
    <img class="headerUserIcon"
         src="../resources/img/user.svg"
         onclick="userMenu()">
</header>

<!-- User menu -->
<div class="userMenu" id="userMenu">
    <img onclick="showHelpModal()"
         src="../resources/img/help.svg"
         title="<?php echo $translation['menu.helpButton.title']; ?>">
    <div class="username">
        <?= htmlspecialchars($_SESSION['username']); ?>
        <?php
        echo '(';
        if ($_SESSION['type'] == 1) {
            echo 'Admin';
        }
        if ($_SESSION['type'] == 0) {
            echo 'Chief Editor';
        }
        if ($_SESSION['type'] == 2) {
            echo 'Editor';
        }
        echo ')';
        ?>
    </div>
    <div>
        <a class="logout" href="logout.php"><?php echo $translation['menu.logout'] ?></a>
    </div>

</div>

<!-- Navigation -->
<aside id="asideNav" class="aside">
    <nav id="navNav" class="nav">
        <?php
        if ($_GET['view'] == 'home') {
            ?>
            <a class="nav-button-active" href=<?= "../view/backend.php?view=home" ?>>
                <span class="dashIcon"><?php echo $translation['menu.dashboard'] ?></span></a>
            <?php
        } else {
            ?>
            <a class="nav-button" href=<?= "../view/backend.php?view=home" ?>>
                <span class="dashIcon"><?php echo $translation['menu.dashboard'] ?></span></a>
            <?php
        }
        ?>
        <?php
        if (isset($_SESSION['type']) && $_SESSION['type'] < 2) {
            ?>
            <?php
            if ($_GET['view'] == 'contenteditor') {
                ?>
                <label for="sites-dropdown">
                    <span class="checkbox-active">
                    <span class="sitesIcon"><?php echo $translation['menu.sites'] ?></span>
                    </span>
                </label><input type="checkbox"
                               id="sites-dropdown">
                <?php
            } else {
                ?>
                <label for="sites-dropdown">
                    <span class="checkbox">
                    <span class="sitesIcon"><?php echo $translation['menu.sites'] ?></span>
                    </span>
                </label><input type="checkbox"
                               id="sites-dropdown">
                <?php
            } ?>
            <ul class="sites-list">
                <?php foreach ($this->_['sites'] as $site) {
                    ?>
                    <li><a class="nav-button"
                           href=<?= "../view/backend.php?view=contenteditor&site=$site" ?>><?= $site ?></a>
                    </li>
                    <?php
                } ?>
            </ul>
            <?php
        } ?>
        <?php
        if ($_GET['view'] == 'posteditor') {
            ?>
            <label for="posts-dropdown">
                    <span class="checkbox-active">
                    <span class="postsIcon"><?php echo $translation['menu.posts'] ?></span>
                    </span>
            </label><input type="checkbox"
                           id="posts-dropdown">
            <?php
        } else {
            ?>
            <label for="posts-dropdown">
                    <span class="checkbox">
                    <span class="postsIcon"><?php echo $translation['menu.posts'] ?></span>
                    </span>
            </label><input type="checkbox"
                           id="posts-dropdown">
            <?php
        }
        ?>
        <ul class="posts-list">
            <?php foreach ($this->_['sites'] as $site) {
                if (FileHandler::getPostsCount($site)) {
                    ?>
                    <li><a class="nav-button"
                           href=<?= "../view/backend.php?view=posteditor&site=$site" ?>><?= $site ?></a>
                    </li>
                    <?php
                }
            } ?>
        </ul>
        <?php
        if (isset($_SESSION['type']) && $_SESSION['type'] < 2) {
            ?>
            <?php
            if ($_GET['view'] == 'languageeditor') {
                ?>
                <a class="nav-button-active" href="../view/backend.php?view=languageeditor">
                    <span class="langIcon"><?php echo $translation['menu.languages'] ?></span></a>
                <?php
            } else {
                ?>
                <a class="nav-button" href="../view/backend.php?view=languageeditor">
                    <span class="langIcon"><?php echo $translation['menu.languages'] ?></span></a>
                <?php
            } ?>
            <?php
        } ?>
        <?php
        if (isset($_SESSION['type']) && $_SESSION['type'] == 1) {
            ?>
            <?php
            if ($_GET['view'] == 'users') {
                ?>
                <a class="nav-button-active" href="../view/backend.php?view=users">
                    <span class="usersIcon"><?php echo $translation['menu.users'] ?></span></a>
                <?php
            } else {
                ?>
                <a class="nav-button" href="../view/backend.php?view=users">
                    <span class="usersIcon"><?php echo $translation['menu.users'] ?></span></a>
                <?php
            } ?>
            <?php
        } ?>
        <?php
        if ($_GET['view'] == 'settings') {
            ?>
            <a class="nav-button-active" href=<?= "../view/backend.php?view=settings" ?>>
                <span class="settingsIcon"><?php echo $translation['menu.settings'] ?></span></a>
            <?php
        } else {
            ?>
            <a class="nav-button" href=<?= "../view/backend.php?view=settings" ?>>
                <span class="settingsIcon"><?php echo $translation['menu.settings'] ?></span></a>
            <?php
        }
        ?>
    </nav>
</aside>

<!-- Renders main content -->
<main id="main" class="content">
    <?php echo $this->_['backend_content']; ?>
</main>

<!-- Display footer on contenteditor, posteditor and languageeditor -->
<?php
if ($_GET['view'] == 'contenteditor' || $_GET['view'] == 'posteditor' || $_GET['view'] == 'languageeditor') {
    echo '<footer></footer>';
}
?>
<script src="../resources/js/navigation.js"></script>
<script src="../resources/js/help.js"></script>
<!--This empty script tag avoid firing the transition animations in Google Chrome after page load (Chrome Bug 332189)-->
<script></script>
</body>
</html>
