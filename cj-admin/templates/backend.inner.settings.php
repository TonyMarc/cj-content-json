<?php
include_once('../controller/ValidationController.php');
include_once('../controller/UserController.php');
include_once('../service/FileHandler.php');
include_once('../service/UploadService.php');

use cj\controller\ValidationController;
use cj\controller\UserController;
use cj\service\FileHandler;
use cj\service\UploadService;

// Include translation config
$translation = include('../config/' . $_SESSION['lang'] . '_lang.php');

$validation = new ValidationController();
$user = new UserController();
$fileHandler = new FileHandler();

if (isset($_POST['languageSetting'])) {
    if ($user->changeBackendLanguage($_SESSION['username'], $_POST['lang'])) {
        $_SESSION['lang'] = $_POST['lang'];
        header('Location: backend.php?view=settings');
        exit;
    }
}

if (isset($_POST['changePassword'])) {
    if ($validation->isUserValid($_SESSION['username'], $_POST['password'])) {
        if ($validation->comparePasswords($_POST['newPassword'], $_POST['newPasswordRepeat'])) {
            $user->changePassword($_SESSION['username'], $validation->createHash($_POST['newPassword']));
            header('Location: backend.php?view=settings');
            exit;
        }
    }
}

if (isset($_POST['uploadFile'])) {
    UploadService::uploadFile();
}

if (isset($_POST['resetLogo'])) {
    UploadService::resetLogo();
}

?>
<h2><?php echo $translation['menu.settings'] ?></h2>

<?php
if ($_SESSION['type'] == 1) {
    ?>
    <div class="card" id="customLogo">
        <div class="cardMainSettings">
            <label><h3><?php echo $translation['settings.customLogo']; ?></h3></label>
        </div>
        <div class="cardFooter">
            <div class="grid">
                <form name="uploadFile" id="uploadFile" action="" method="post" enctype="multipart/form-data">
                    <span><?php echo $translation['settings.customLogo.fileInfo']; ?></span>
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input id="uploadImage" name="uploadFile" type="submit"
                           value="<?php echo $translation['settings.customLogo.uploadImage'] ?>">
                </form>

                <div class="logoResetForm">
                    <img id="logo" src="../resources/img/logo.png">
                    <form name="resetLogo" action="" class="form-resetLogo" method="post">
                        <button name="resetLogo"
                                type="submit"
                                id="resetLogo"
                                value="update"><?php echo $translation['button.reset']; ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<div class="card">
    <div class="cardMainSettings">
        <label><h3><?php echo $translation['setting.languageSetting']; ?></h3></label>
    </div>

    <div class="cardFooter">
        <form name="languageSetting" action="" class="form-languageSetting" method="post">

            <div class="backendLanguage">
                <select name="lang"
                        title="Select your preferred language."
                        required="required"
                        id="backendLanguage">
                    <?php
                    $config = include('../config/config.php');
                    foreach ($config['translations'] as $lang) {
                        echo '<option value="' . $lang . '"';
                        if ($_SESSION['lang'] == $lang) {
                            echo "selected";
                        }
                        $translationKey = 'languages.' . $lang;
                        echo '>' . $translation[$translationKey] . '</option>';
                    }
                    ?>
                </select>
            </div>

            <button type="submit"
                    class="saveButton"
                    name="languageSetting"
                    id="changeBackendLanguage"
                    value="update"><?php echo $translation['button.save']; ?></button>
        </form>
    </div>
</div>

<div class="card">

    <div class="cardMainSettings">
        <label><h3><?php echo $translation['settings.changePassword'] ?></h3></label>
    </div>

    <div class="cardFooter">
        <form name="changePassword" action="" class="form-changePassword" method="post" autocomplete="off">
            <?php
            // Output of error messages
            echo $user->getErrors();
            echo '<br>';
            echo $validation->getErrors();
            ?>
            <div class="formGroup">
                <input type="password"
                       placeholder="<?php echo $translation['form.password.placeholder']; ?>"
                       name="password"
                       id="inputPw"
                       required="required"
                       title="<?php echo $translation['form.password.title']; ?>"
                       autocomplete="off">
                <input type="password"
                       placeholder="<?php echo $translation['form.newPassword.placeholder']; ?>"
                       name="newPassword"
                       required="required"
                       pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                       title="<?php echo $translation['form.newPassword.title']; ?>"
                       id="inputNewPw"
                       autocomplete="off">
                <input type="password"
                       placeholder="<?php echo $translation['form.repeatPassword.placeholder']; ?>"
                       name="newPasswordRepeat"
                       required="required"
                       pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                       title="<?php echo $translation['form.repeatPassword.title']; ?>"
                       autocomplete="off"
                       onfocus="comparePasswords(this)">

                <button name="changePassword"
                        class="saveButton"
                        type="submit"
                        id="changePassword"
                        value="update"><?php echo $translation['button.save']; ?></button>
            </div>
        </form>
    </div>
</div>
<script src="../resources/js/pattern.js"></script>
