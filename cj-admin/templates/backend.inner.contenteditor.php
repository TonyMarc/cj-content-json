<?php

use cj\service\JsonService;

// Include translation config
$translation = include ('../config/'. $_SESSION['lang'] .'_lang.php');

if ($_SESSION['type'] < 2) {
?>

<div>
    <h2><?= $this->_['site'] ?></h2>
    <form class="language-select-form" action="" method="get">
        <?php
        // we want to keep the get parameters when submitting the form
        foreach ($_GET as $name => $value) {
            $name = htmlspecialchars($name);
            $value = htmlspecialchars($value);
            if ($name != 'lang') {
                echo '<input type="hidden" name="' . $name . '" value="' . $value . '">';
            }
        }
        ?>
        <select name="lang"
                size="1"
                onchange="this.form.submit()"
                title="<?php echo $translation['contenteditor.languageVersion']; ?>">
            <?php
            $selectedLang = isset($_GET['lang']) ? $_GET['lang'] : JsonService::getDefaultLanguage();
            $translationKey = '';
            foreach ($this->_['languages'] as $language) {
                $translationKey = 'languages.' . $language['short'];
                ?>
                <option value="<?= $language['short'] ?>"
                  <?php echo $language['short'] == $selectedLang ? "selected" : "" ?>><?php if ($translation[$translationKey] != '') {
                      echo $translation[$translationKey];
                    } else {
                      echo $language['long'];
                    }
                    ?></option>
            <?php
            } ?>
        </select>
    </form>
</div>

<!-- Modal dialog -->
<div id="modalAddDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeAddDialog">&times;</span>
        <h3><?php echo $translation['contenteditor.addNew']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="addNo"><?php echo $translation['button.no']; ?></button>
            <button type="button"
                    id="addYes"><?php echo $translation['button.yes']; ?></button>
        </div>
    </div>
</div>
<div id="modalDeleteDialog" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close"
              id="closeDeleteDialog">&times;</span>
        <h3><?php echo $translation['contenteditor.deleteContentElement']; ?></h3>
        <div class="buttonsModal">
            <button type="button"
                    id="deleteNo"><?php echo $translation['button.no']; ?></button>
            <button type="button"
                    id="deleteYes"><?php echo $translation['button.yes']; ?></button>
        </div>
    </div>
</div>
<form action="" method="post">
    <?php
    foreach ($this->_['content'] as $content) {
        if (isset($content['updated'])) {
            break;
        } ?>
        <div class="card">
            <img src="../resources/img/delete.svg"
                 id="deleteButton"
                 title="<?php echo $translation['contenteditor.deleteButton.title']?>"
                 class="remove"
                 onclick="showDeleteModal(this)">
            <img src="../resources/img/addButton.svg"
                 id="addButton"
                 class="add"
                 title="<?php echo $translation['contenteditor.addButton.title']?>"
                 onclick="showAddModal(this)">
                <div class="cardMain">
                    <label><h3><?= $content['parent'] ?></h3></label>
                    <input type="hidden" name="modified_content_parents[]" value="<?= $content['parent'] ?>"/>
                </div>
                <div class="cardFooter">
                    <textarea rows="5" placeholder="<?php echo $translation['contenteditor.textarea.placeholder']; ?>" name="modified_content[]"><?= $content['content'] ?></textarea></div>
        </div>
    <?php
    } ?>

    <div class="save">
    <button type="submit"><?php echo $translation['button.save']; ?></button>
    </div>
</form>
    <?php
} else {
    header('Location: backend.php?');
    exit;
}
?>
<script src="../resources/js/contenteditor.js"></script>

