<?php
session_start();
include_once('../controller/ValidationController.php');
include_once('../controller/RegistrationController.php');
include_once('../model/View.php');

use cj\controller\ValidationController;
use cj\controller\RegistrationController;

$validation = new ValidationController();
$registration = new RegistrationController();

// Validation of registration data
if (!file_exists("../etc/.shadow")) {
    if (empty($_POST['Id'])) {
        if (isset($_POST['username'])) {
            if ($validation->comparePasswords($_POST['password'], $_POST['passwordRepeat'])) {
                if ($registration->createUser($_POST['username'], $validation->createHash($_POST['password']), 1, 'en',
                    '1')) {
                    header('Location: login.php?');
                    exit;
                }
            }
        }
    }
} else {
    header('Location: login.php?');
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CJ</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resources/img/favicon.ico">
    <link rel="stylesheet" href="../resources/css/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
</header>
<div class="main">
    <div>
        <div class="wrapper">
            <div class="card">
                <form action="" class="form-login" method="post" autocomplete="off">
                    <div class="cardMain">

                        <img id="logo" src="../resources/img/logo.png">
                        <!--<img id="logo" src="../resources/img/logo.svg">-->

                        <div class="formGroup">
                            <?php
                            // Output error messages
                            echo '<p class="errorMessage">' . $registration->getErrors() . '</p>';
                            echo '<p class="errorMessage">' . $validation->getErrors() . '</p>';
                            $validation->errorMessage = '';
                            $registration->errorMessage = '';
                            ?>
                            <input id="inputId"
                                   type="text"
                                   placeholder="Id"
                                   name="Id">
                            <input type="text"
                                   placeholder="Username"
                                   name="username"
                                   id="inputName"
                                   required="required"
                                   pattern="[^:\s]{3,}"
                                   title="Minimum three characters, no whitespaces, no colon (:) character">
                            <input type="password"
                                   placeholder="Password"
                                   name="password"
                                   required="required"
                                   pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                                   title="Minimum eight characters, at least one number/letter."
                                   id="inputPw">
                            <input type="password"
                                   placeholder="Repeat Password"
                                   name="passwordRepeat"
                                   required="required"
                                   pattern="^(?=.*[a-z])(?=.*\d).{8,}$"
                                   title="Repeat your password."
                                   onfocus="compareNewUserPasswords(this)">
                            <button type="submit" id="submit-login">Register new user</button>
                        </div>
                    </div>
                    <div class="cardFooter">
                        <a href="http://contentjson.org/" target="_blank">Content JSON</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
    <script src="../resources/js/login.js"></script>
    <script src="../resources/js/pattern.js"></script>
</footer>
</body>
</html>
