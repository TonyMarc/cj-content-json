<?php
include_once('../model/View.php');

use cj\controller\ValidationController;

if (!isset($_GET['report'])) {
    header('Location: login.php?');
    exit;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CJ | Content JSON</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resources/img/favicon.ico">
    <link rel="stylesheet" href="../resources/css/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
</header>
<div class="main">
    <div>
        <div class="wrapper">
            <div class="card">
                <form action="" class="form-login" method="post" autocomplete="off">

                    <div class="cardMain">

                        <img id="logo" src="../resources/img/logo.png">
                        <!--<img id="logo" src="../resources/img/logo.svg">-->
                        <?php
                        if ($_GET['report'] == 1) {
                            ?>
                            <div class="formGroup">
                                <div class="prompt">Thank you for registering. Your account <i><?php echo $_GET['user'] ?></i> needs to be
                                    activated. Please contact your administrator.
                                </div>

                            </div>
                            <?php
                        }
                        if ($_GET['report'] == 300) {

                            ?>
                            <div class="prompt">Your registration link is not valid. Please contact your administrator.</div>

                        <?php
                        }
                        ?>

                    </div>
                    <div class="cardFooter">
                        <a href="http://contentjson.org/" target="_blank">Content JSON</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
</footer>
</body>
</html>
