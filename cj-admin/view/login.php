<?php
session_start();
include_once('../controller/ValidationController.php');
include_once('../model/View.php');

use cj\controller\ValidationController;

$validation = new ValidationController();
if (!isset($_SESSION['username'])) {
    // Validation of login data
    if (file_exists("../etc/.shadow")) {
        if (empty($_POST['Id'])) {
            if (isset($_POST['username'])) {
                // valid login
                if ($validation->isUserActive($_POST['username'])) {
                    if ($validation->isUserValid(
                        $_POST['username'],
                        $_POST['password']
                    )) {
                        $_SESSION['username'] = $_POST['username'];
                        $_SESSION['type'] = $validation->userData($_POST['username'], 'type');
                        $_SESSION['lang'] = $validation->userData($_POST['username'], 'lang');
                        $_SESSION['tag'] = $validation->userData($_POST['username'], 'tag');
                        header('Location: backend.php?');
                        exit;
                    }
                }
            }
        }
    } else {
        header('Location: registration.php?');
        exit;
    }
} else {
    header('Location: backend.php?');
    exit;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CJ | Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resources/img/favicon.ico">
    <link rel="stylesheet" href="../resources/css/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<header>
</header>
<div class="main">
    <div>
        <div class="wrapper">
            <div class="card">
                <form action="" class="form-login" method="post" autocomplete="off">
                    <div class="cardMain">

                        <img id="logo" src="../resources/img/logo.png">
                        <!--<img id="logo" src="../resources/img/logo.svg">-->

                        <div class="formGroup">

                            <?php
                            // Output error messages
                            echo '<p class="errorMessage">' . $validation->getErrors() . '</p>';
                            echo '<p class="errorMessage">' . $validation->getLoginErrors() . '</p>';
                            $validation->errorMessage = '';
                            $validation->loginErrorMessage = '';
                            ?>
                            <input id="inputId"
                                   type="text"
                                   placeholder="Id"
                                   name="Id">
                            <input id="inputName"
                                   type="text"
                                   placeholder="Username"
                                   name="username"
                                   required="required"
                                   pattern="^.{3,}$"
                                   title="At least three characters">
                            <input id="inputPw"
                                   type="password"
                                   placeholder="Password"
                                   name="password"
                                   required="required"
                                   pattern="^.{8,}$"
                                   title="At least eight characters">
                            <button type="submit" id="submit-login">Login</button>
                        </div>
                    </div>
                    <div class="cardFooter">
                        <a href="http://contentjson.org/" target="_blank">Content JSON</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
</footer>
</body>
</html>
