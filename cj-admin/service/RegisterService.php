<?php

namespace cj\service;

/**
 * Class RegisterService
 *
 * @package cj\service
 */
class RegisterService
{
    /**
     * Holds items from .otp file
     *
     * @var array
     */
    public $credentials = array();

    /**
     * Get credentials array
     *
     * @return array
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * Creates link hash in .otp file
     *
     * @param string $type
     * @param string $lang
     * @return bool
     */
    public static function createLinkHash($type, $lang)
    {
        $time = time();
        $hash = mt_rand();
        $linkHash = $hash . $time . ':' . $type . ':' . $lang;

        if (file_exists("../etc/shade/.otp")) {
            file_put_contents("../etc/shade/.otp", $linkHash . PHP_EOL, FILE_APPEND | LOCK_EX);
            return true;
        } else {
            file_put_contents("../etc/shade/.otp", $linkHash . PHP_EOL, FILE_APPEND | LOCK_EX);
            return true;
        }
    }

    /**
     * Returns link hashes
     *
     * @return array|bool
     */
    public function getLinkHashes()
    {
        if (file_exists("../etc/shade/.otp")) {
            $fn = fopen("../etc/shade/.otp", "r") or die("failed to open file");

            while ($row = fgets($fn)) {
                list($link, $type, $lang) = explode(":", $row);

                $this->credentials[] = array(
                    'link' => $link,
                    'type' => $type,
                    'lang' => $lang,
                );
            }
            fclose($fn);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Returns true if link hash is found
     *
     * @param string $hash
     * @return bool
     */
    public function isHashValid($hash)
    {
        $this->getLinkHashes();
        $hashes = $this->getCredentials();
        $pos = null;
        foreach ($hashes as $key => $value) {
            if ($value['link'] === $hash) {
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes link hash
     *
     * @param $link
     * @return bool returns true if the deletion was successful
     */
    public function deleteHash($link)
    {
        if (!empty($link)) {
            $f = file_get_contents("../etc/shade/.otp");

            $otps = explode(PHP_EOL, $f);

            $key = $this->substringInArray($link, $otps);

            unset($otps[$key]);
            file_put_contents("../etc/shade/.otp", implode(PHP_EOL, $otps), LOCK_EX);
            if (!filesize("../etc/shade/.otp")) {
                unlink("../etc/shade/.otp");
            }
            return true;
        } else {
            echo 'Something went wrong.';
            return false;
        }
    }

    /**
     * Returns hash data attribute 'type' or 'lang'
     *
     * @param $link
     * @param $attribute
     * @return bool|mixed
     */
    public function getHashData($link, $attribute)
    {
        $this->getLinkHashes();
        $otps = $this->getCredentials();
        $pos = null;
        foreach ($otps as $key => $value) {
            if ($value['link'] === $link) {
                $pos = $key;
                return preg_replace('/\s+/', '', $otps[$pos][$attribute]);
            }
        }
        return false;
    }


    /**
     * Creates .config file for user domain
     *
     * @param string $domain
     * @return bool
     */
    public static function createDomainConfig($domain)
    {

        if (!file_exists("../etc/shade/.config")) {
            $fp = fopen('../etc/shade/.config', 'w');
            fwrite($fp, $domain);
            fclose($fp);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Loads .config file
     *
     * @return bool|string
     */
    public static function getDomainConfig()
    {
        if (file_exists("../etc/shade/.config")) {
            $file = file_get_contents("../etc/shade/.config");
            return $file;
        }
        return false;
    }

    /**
     * @return string
     */
    public static function getDomainName()
    {
        $domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $result = explode('/cj-admin', $domain);
        return $result[0];
    }

    /**
     * @param $domain
     * @param $hash
     * @return string
     */
    public static function createRegistrationLink($domain, $hash)
    {
        $link = $domain . '/cj-admin/view/register.php?hash=' . $hash;
        return $link;
    }

    /**
     * Checks for all strings in the values of an associative
     * array and returns the according key
     *
     * @param string $string the string to search for
     * @param array $array the associative array to look through
     * @return bool|int|string returns the key to the value
     * or false if the value could not be found.
     */
    private function substringInArray($string, array $array)
    {
        foreach ($array as $key => $value) {
            if (strpos($value, $string) !== false) {
                return $key;
            }
        }
        return false;
    }

}
