<?php
namespace cj\service;

require_once('../service/FileHandler.php');

use DOMDocument;
use Exception;
use cj\service\FileHandler as FileHandlerService;

/**
 * Class JsonService
 *
 * Handles JSON CRUD
 */
class JsonService
{
    /**
     * Creates JSON files from given website file (e.g. html)
     *
     * @param string $file
     * @param string $lang
     */
    public static function createContentJsonFile($file, $lang)
    {
        $config = include('../config/config.php');
        $path = "../.." . $config['sitesContent'];

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTMLFile("../../" . $file);
        // No error output due to html 5 elements
        libxml_clear_errors();

        try {
            $elements = $doc->getElementsByTagName('span');
        } catch (Exception $e) {
            // TODO handle error
        }
        if (!is_null($elements)) {
            $array = array();

            foreach ($elements as $element) {
                $class = $element->getAttribute('class');
                if ($class == 'cj') {
                    $parentNode = $element->parentNode;
                    $parent = $parentNode->nodeName;
                    $content = null;

                    $nodes = $element->childNodes;
                    foreach ($nodes as $node) {
                        $content = $node->nodeValue;
                    }

                    $array[] = array(
                      'content' => $content,
                      'parent' => $parent,
                    );
                }
            }
            // Add timestamp
            $date = array(array('updated' => time()));
            $jsonArray = array_merge($array, $date);

            $filename = self::beautifyFilename($file);

            // Create directory structure if it does not exists
            if (!file_exists($path . $filename) && !is_dir($path)) {
                mkdir($path, 0755, true);
            }
            // Create JSON file
            $fp = fopen($path . $filename . "_" . $lang . '.json', 'w');
            fwrite($fp, json_encode($jsonArray));
            fclose($fp);
        }
    }

    /**
     * Loads JSON file and returns array
     *
     * @param string $file
     * @param string $lang
     * @return array|bool
     */
    public static function loadContentJsonFile($file, $lang)
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];

        $filename = self::beautifyFilename($file);

        // Read JSON file
        if (file_exists("../.." . $path . $filename . '_' . $lang . '.json')) {
            $json = file_get_contents("../.." . $path . $filename . '_' . $lang . '.json');
        } else {
            // TODO handle error
            self::createContentJsonFile($file, $lang);
            return self::loadContentJsonFile($file, $lang);
        }

        //Decode JSON
        $json_data = json_decode($json, true);

        return (array)$json_data;
    }

    /**
     * Updates JSON files and adds a new timestamp
     *
     * @param array $json a correctly formatted json
     * @param string $file the name of the site file, e.g. index.html
     * @param string $lang the short tag for the language
     */
    public static function updateContentJsonFile($json, $file, $lang)
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];

        $filename = self::beautifyFilename($file);

        if (isset($json['updated'])) {
            // Removes timestamp
            array_pop($json);
        }
        // Adds new timestamp
        $date = array(array('updated' => time()));
        $jsonArray = array_merge($json, $date);
        $jsonData = json_encode($jsonArray);

        file_put_contents("../.." . $path . $filename . '_' . $lang . '.json', $jsonData);
    }

    /**
     * Formats return array (content)
     *
     * @param array $contentArray an array of all content
     * @param array $parentArray and array of all parent element tags
     * @return array the converted json string or null if an error occurred
     */
    public static function formatArraysForContentJson($contentArray, $parentArray)
    {
        if (count($contentArray) == count($parentArray)) {
            $result = array();
            for ($i = 0; $i < count($contentArray); $i++) {
                $result[] = array(
                  'content' => $contentArray[$i],
                  'parent' => $parentArray[$i],
                );
            }
            return $result;
        } else {
            // TODO throw error
            return null;
        }
    }

    /**
     * Deletes a specific content JSON file
     *
     * @param $file
     * @param $lang
     * @return bool
     */
    public static function deleteContentJsonFile($file, $lang)
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];
        $filename = self::beautifyFilename($file);

        if (file_exists("../.." . $path . $filename . '_' . $lang . '.json')) {
            unlink("../.." . $path . $filename . '_' . $lang . '.json');
            return true;
        } else {
            // TODO handle error
            return false;
        }
    }

    /**
     * Deletes all content files of the given language
     *
     * @param $lang
     * @return bool
     */
    public static function deleteAllContentJsonForLanguage($lang)
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];
        $handler = new FileHandlerService();
        $files = $handler->getHtmlFiles();

        foreach ($files as $file) {
            $file = self::beautifyFilename($file);
            if (file_exists("../.." . $path . $file . '_' . $lang . '.json')) {
                unlink("../.." . $path . $file . '_' . $lang . '.json');
            }
        }
        return true;
    }

    /**
     * Duplicates an existing JSON file
     *
     * @param $file
     * @param $defaultLang
     * @param $newLang
     * @return bool
     */
    public static function duplicateContentJsonFile($file, $defaultLang, $newLang)
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];
        $filename = self::beautifyFilename($file);

        if (file_exists("../.." . $path . $filename . '_' . $defaultLang . '.json')) {
            copy(
                "../.." . $path . $filename . '_' . $defaultLang . '.json',
              "../.." . $path . $filename . '_' . $newLang . '.json'
            );
            return true;
        } else {
            // TODO handle error
            return false;
        }
    }

    /**
     * This method returns all existing content json
     * files at the default location as an array of strings
     *
     * @return array all existing content json files
     */
    public static function listContentJsonFiles(): array
    {
        $config = include('../config/config.php');
        $path = $config['sitesContent'];
        $result = array();
        foreach (glob('../..' . $path . '/*.*') as $file) {
            preg_match('/.*\/(\w+_\w+\.\w+)$/', $file, $matches);
            $result[] = self::beautifyFilename($matches[1]);
        }
        return $result;
    }

    /**
     * Returns file name without file extension
     *
     * @param string $file
     * @return string
     */
    private static function beautifyFilename($file)
    {
        $s = explode(".", $file);
        $beautifiedFilename = $s[0];
        return $beautifiedFilename;
    }

    /**
     * This method creates a new language.json file
     */
    public static function createLanguageJsonFile()
    {
        $config = include('../config/config.php');
        $path = "../.." . $config['languagePath'];
        $filename = 'language.json';
        if (!file_exists($path . $filename) && !is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $fp = fopen($path . $filename, 'w');
        $defaultLanguages = '[{"long":"English","short":"en", "isDefault":1, "isActive":1}, 
                            {"long":"German","short":"de", "isDefault":0, "isActive":1}, 
                            {"long":"Français","short":"fr", "isDefault":0, "isActive":0}]';
        fwrite($fp, $defaultLanguages);
        fclose($fp);
    }

    /**
     * This method updates the json file. It is called when a Language
     * is deleted or added.
     *
     * @param array
     * TODO
     */
    public static function updateLanguageJsonFile($request)
    {
        $config = include('../config/config.php');
        $path = $config['languagePath'];
        $filename = 'language.json';
        // if the language.json has been deleted or cannot be found, we create it again.
        if (!file_exists('../..' . $path . $filename)) {
            self::createLanguageJsonFile();
        }

        $result = array();
        // handle language update
        if (isset($request['long']) && isset($request['short']) && count($request['long']) == count($request['short'])) {
            $isDefault = $request['isDefault'];
            for ($i = 0; $i < count($request['short']); $i++) {
                // add the language to the result if we don't want to delete it
                $addLanguage = true;
                if (isset($request['toDelete'])) {
                    foreach ($request['toDelete'] as $toDelete) {
                        if ($request['short'][$i] == $toDelete) {
                            self::deleteAllContentJsonForLanguage($request['short'][$i]);
                            $addLanguage = false;
                            break;
                        }
                    }
                }
                if ($addLanguage) {
                    $setActive = false;
                    foreach ($request['isActive'] as $isActive) {
                        if ($request['short'][$i] == $isActive) {
                            $setActive = true;
                            break;
                        }
                    }
                    $result[] = array(
                      "long" => $request['long'][$i],
                      "short" => $request['short'][$i],
                      "isDefault" => $request['short'][$i] == $isDefault ? 1 : 0,
                      "isActive" => $setActive ? 1 : 0,
                    );
                    if (isset($request['new'])) {
                        $fh = new FileHandlerService();
                        $filesToCreate = $fh->getHtmlFiles();
                        foreach ($request['new'] as $langShort) {
                            if ($langShort == $request['short'][$i]) {
                                foreach ($filesToCreate as $file) {
                                    self::createContentJsonFile($file, $langShort);
                                }
                            }
                        }
                    }
                }
            }
            $config = include('../config/config.php');
            $path = $config['languagePath'];
            $filename = 'language.json';
            if (count($result) > 0) {
                file_put_contents("../.." . $path . $filename, json_encode($result));
            }
        }
    }

    /**
     * This methods loads the content of the language.json file
     *
     * @return array
     */
    public static function loadLanguages()
    {
        $config = include('../config/config.php');
        $path = $config['languagePath'];
        $filename = 'language.json';
        // if the language.json has been deleted or cannot be found, we create it again.
        if (!file_exists('../..' . $path . $filename)) {
            self::createLanguageJsonFile();
        }
        $json = file_get_contents("../.." . $path . $filename);
        $json_data = json_decode($json, true);

        return (array)$json_data;
    }

    /**
     * This method returns the short tag for the set default language
     *
     * @return mixed the short tag of the default language
     */
    public static function getDefaultLanguage()
    {
        foreach (self::loadLanguages() as $language) {
            if ($language['isDefault'] == 1) {
                return $language['short'];
            }
        }
        //TODO show error - no default language could be detected. Please contact your server admin.
        // The language.json seems to be corrupted.
    }

    /**
     * Creates JSON post file from given website file (e.g. html)
     * Does NOT parse html file, due to the dynamic nature of this
     * feature, the post content.json files are expected to follow
     * this specific syntax
     *
     * @param string $file
     * @param string $lang
     * @return bool
     */
    public static function createPostJsonFile($file, $lang)
    {
        // Checks for cj post classes
        $postCount = FileHandler::getPostsCount($file);
        if ($postCount > 0) {
            $config = include('../config/config.php');
            $path = "../.." . $config['postsContent'];

            $array[] = array();
            for ($i = 0; $i < $postCount; $i++) {
                $array[$i] = array(
                  'headline' => '',
                  'post' => '',
                  'date' => ''
                );
            }

            // Add timestamp
            $date = array(array('updated' => time()));
            $jsonArray = array_merge($array, $date);

            $filename = self::beautifyFilename($file);

            // Create directory structure if needed
            if (!file_exists($path . $filename) && !is_dir($path)) {
                mkdir($path, 0755, true);
            }
            // Create JSON file
            $fp = fopen($path . $filename . "_" . $lang . '.json', 'w');
            fwrite($fp, json_encode($jsonArray));
            fclose($fp);
        }
        return false;
    }

    /**
     * Loads JSON post file and returns array
     *
     * @param string $file
     * @param string $lang
     * @return array|bool
     */
    public static function loadPostJsonFile($file, $lang)
    {
        $config = include('../config/config.php');
        $path = $config['postsContent'];

        $filename = self::beautifyFilename($file);

        // Read JSON file
        if (file_exists("../.." . $path . $filename . '_' . $lang . '.json')) {
            $json = file_get_contents("../.." . $path . $filename . '_' . $lang . '.json');
        } else {
            // TODO handle error
            self::createPostJsonFile($file, $lang);
            return self::loadPostJsonFile($file, $lang);
        }

        //Decode JSON
        $json_data = json_decode($json, true);

        return (array)$json_data;
    }

    /**
     * Updates JSON post files and adds a new timestamp
     *
     * @param array $json a correctly formatted json
     * @param string $file the name of the site file, e.g. index.html
     * @param string $lang the short tag for the language
     */
    public static function updatePostJsonFile($json, $file, $lang)
    {
        $config = include('../config/config.php');
        $path = $config['postsContent'];

        $filename = self::beautifyFilename($file);

        if (isset($json['updated'])) {
            // Removes timestamp
            array_pop($json);
        }
        // Adds new timestamp
        $date = array(array('updated' => time()));
        $jsonArray = array_merge($json, $date);
        $jsonData = json_encode($jsonArray);

        file_put_contents("../.." . $path . $filename . '_' . $lang . '.json', $jsonData);
    }

    /**
     * Formats return array (post)
     *
     * @param array $headlineArray
     * @param array $postArray
     * @param array $dateArray
     * @return array|null
     */
    public static function formatArraysForPostJson($headlineArray, $postArray, $dateArray)
    {
        if (count($headlineArray) == count($postArray)) {
            $result = array();
            for ($i = 0; $i < count($headlineArray); $i++) {
                $result[] = array(
                  'headline' => $headlineArray[$i],
                  'post' => $postArray[$i],
                  'date' => $dateArray[$i]
                );
            }
            return $result;
        } else {
            // TODO throw error
            return null;
        }
    }
}
