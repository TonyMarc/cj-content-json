<?php
namespace cj\service;

/**
 * Class AnalyticsService
 */
class AnalyticsService
{

    /**
     * @param $data
     */
    public static function loadAnalyticsFile($data)
    {
        $config = include ('../config/config.php');

        // Create directory structure if needed
        if (!file_exists($config['analyticsPath'] . 'test.txt') && !is_dir($config['analyticsPath'])) {
            mkdir($config['analyticsPath'], 0755, true);
        }

        $fp = fopen($config['analyticsPath'] . 'test.txt', 'w');
        fwrite($fp, $data);
        fclose($fp);
    }
}
