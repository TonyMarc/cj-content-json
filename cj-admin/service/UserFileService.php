<?php

namespace cj\service;

/**
 * Class UserFileService
 *
 * @package cj\service
 */
class UserFileService
{
    /**
     * Checks for all strings in the values of an associative
     * array and returns the according key
     *
     * @param string $string the string to search for
     * @param array $array the associative array to look through
     * @return bool|int|string returns the key to the value
     * or false if the value could not be found.
     */
    public static function substringInArray($string, array $array)
    {
        foreach ($array as $key => $value) {
            if (strpos($value, $string) !== false) {
                return $key;
            }
        }
        return false;
    }

    /**
     * Formats the username, password hash, user type and backend
     * language according to .shadow file layout
     *
     * @param string $username the given username
     * @param string $passwordHash the given password hash
     * @param string $userType the given user type
     * @param string $backendLanguage
     * @param $tagSetting
     * @return string
     */
    public static function beautifyString($username, $passwordHash, $userType, $backendLanguage, $tagSetting)
    {
        $beautifiedString = $username . ':' . $passwordHash . ':' . $userType . ':' . $backendLanguage . ':' . $tagSetting;
        return $beautifiedString;
    }

    /**
     * Checks if the username is already present in .shadow
     *
     * @param string $username
     * @return bool true if the username already exists
     */
    public static function isUsernameAlreadyTaken($username)
    {
        $shadows = self::getUsers();
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $username) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method returns an associative array of all
     * available users from the .shadow file
     *
     * @return array|bool an array of all registered users or false if the file does not exist
     */
    public static function getUsers()
    {
        if (file_exists("../etc/.shadow")) {
            $fn = fopen("../etc/.shadow", "r") or die("failed to open file");

            $credentials = array();
            while ($row = fgets($fn)) {
                list($username, $pw, $type) = explode(":", $row);

                $credentials[] = array(
                    'username' => $username,
                    'password' => $pw,
                    'type' => $type,
                );
            }
            fclose($fn);
            return $credentials;
        } else {
            return false;
        }
    }
}
