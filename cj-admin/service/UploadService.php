<?php

namespace cj\service;

/**
 * Class UploadService
 *
 * @package cj\service
 */
class UploadService
{
    /**
     * Uploads and replaces logo file of the login screen
     */
    public static function uploadFile()
    {
        $target_dir = "../resources/img/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["uploadFile"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                echo '<span class="infoMessage">File is not an image.</span>';
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo '<span class="infoMessage">Sorry, file already exists.</span>';
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 2300000) {
            echo '<span class="infoMessage">Image files must no be larger than 2MB.</span>';
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "png") {
            echo '<span class="infoMessage">Sorry, only PNG files are allowed.</span>';
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo '<span class="infoMessage">Your file was not uploaded.</span>';
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo '<span class="infoMessage">The file <i>'. basename( $_FILES["fileToUpload"]["name"]). '</i> has been uploaded.</span>';

                if (!file_exists($target_dir . 'oldLogo.png')){
                    rename($target_dir . 'logo.png', $target_dir . 'oldLogo.png');
                    rename($target_file, $target_dir . 'logo.png');
                }elseif (file_exists($target_dir . 'oldLogo.png')){
                    rename($target_file, $target_dir . 'logo.png');
                }
            } else {
                echo '<span class="infoMessage">Sorry, an error occurred.</span>';
            }
        }
    }

    /**
     * Resets the logo of the login screen
     */
    public static function resetLogo()
    {
        $target_dir = "../resources/img/";
        $oldLogo = 'oldLogo.png';

        if (file_exists($target_dir . $oldLogo)){
            rename($target_dir . $oldLogo, $target_dir . 'logo.png');
        }
    }
}
