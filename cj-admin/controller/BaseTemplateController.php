<?php

namespace cj\controller;

/**
 * Class BaseTemplateController
 *
 * This class serves as a base for all controllers associated with displaying templates and provides common features
 * helpful for all controllers
 */
abstract class BaseTemplateController
{
    /**
     * @var null This variable holds the given request data
     */
    protected $request = null;

    /**
     * @var string this variable holds the template
     */
    protected $template = '';

    /**
     * the BaseController constructor sets the request and the template if existent
     *
     * @param $request
     */
    public function __construct($request)
    {
        // check if a request has been passed.
        $this->request = sizeof($request) === 0 ? null : $request;
        $this->template .= !empty($request['view']) ? $request['view'] : 'home';
    }

    /**
     * This method handles the assignment of data for templates and assigns templates to views
     *
     * @return mixed
     */
    abstract protected function display();

    /**
     * This method serves as an access point to handle any request data
     *
     * @return mixed
     */
    abstract public function handleGetOrPost();
}
