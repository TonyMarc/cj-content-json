<?php

namespace cj\controller;

require_once ('../service/UserFileService.php');

use cj\service\UserFileService;

/**
 * Class RegistrationController
 *
 * This class handles addition and deletion of users to .shadow file
 */
class RegistrationController
{
    /**
     * @var string
     */
    public $errorMessage = '';

    /**
     * This method checks the given username and password and
     * saves the user if the given input is valid
     *
     * @param string $username
     * @param string $passwordHash
     * @param string $userType
     * @param string $backendLanguage
     * @param $tagSetting
     * @return bool true if the user creation was successful
     */
    public function createUser($username, $passwordHash, $userType, $backendLanguage, $tagSetting)
    {
        if (strlen($username) >= 3 && !preg_match('/\s/',$username) && !preg_match('/:/', $username)) {
            if (!empty($passwordHash)) {
                if (file_exists("../etc/.shadow")) {
                    if (UserFileService::isUsernameAlreadyTaken($username)) {
                        $this->errorMessage = 'Username already taken';
                        return false;
                    } else {
                        $newUser = UserFileService::beautifyString($username, $passwordHash, $userType, $backendLanguage,
                            $tagSetting);
                        file_put_contents("../etc/.shadow", $newUser . PHP_EOL, FILE_APPEND | LOCK_EX);
                        return true;
                    }
                } else {
                    $newUser = UserFileService::beautifyString($username, $passwordHash, $userType, $backendLanguage,
                        $tagSetting);
                    file_put_contents("../etc/.shadow", $newUser . PHP_EOL, FILE_APPEND | LOCK_EX);
                    return true;
                }
            } else {
                $this->errorMessage = "Password: Minimum eight characters, at least one number/letter.";
                return false;
            }
        } else {
            $this->errorMessage = 'Username: At least 3 characters, no whitespaces, no colon character (:).';
            return false;
        }
    }

    /**
     * This method handles the deletion of users.
     * The last admin user cannot be deleted and a user cannot delete herself
     *
     * @param string $username the user to be deleted
     * @return bool returns true if the deletion was successful
     */
    public function deleteUser($username)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);

            $tmp = array_filter($shadows);

            if (count($tmp) == 1 || $_SESSION['username'] === $username) {
                $this->errorMessage = 'At least one admin user must be present.';
                return false;
            } else {
                $key = UserFileService::substringInArray($username, $shadows);

                unset($shadows[$key]);
                file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
                echo '<span class="infoMessage">User ' . $username . ' was deleted</span>';
                return true;
            }
        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Returns error messages
     *
     * @return string
     */
    public function getErrors()
    {
        return $this->errorMessage;
    }
}
