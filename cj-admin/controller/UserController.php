<?php

namespace cj\controller;

require_once ('../service/UserFileService.php');

use cj\service\UserFileService;

/**
 * Class UserController
 *
 * @package cj\controller
 */
class UserController
{
    /**
     * @var string
     */
    public $errorMessage = '';

    /**
     * Changes the password of a user
     *
     * @param string $username
     * @param string $passwordHash
     * @return bool returns true if password was changed
     */
    public function changePassword($username, $passwordHash)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = UserFileService::substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = UserFileService::beautifyString($tmp[0], $passwordHash, $tmp[2], $tmp[3], $tmp[4]);

            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            echo '<span class="infoMessage">Your password was changed.</span>';
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Changes the preferred back-end language of a user
     *
     * @param string $username
     * @param string $backendLanguage
     * @return bool returns true if language was changed
     */
    public function changeBackendLanguage($username, $backendLanguage)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = UserFileService::substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = UserFileService::beautifyString($tmp[0], $tmp[1], $tmp[2], $backendLanguage, $tmp[4]);
            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Change user role
     *
     * @param string $username
     * @param $userType
     * @return bool returns true if tag was changed
     */
    public function changeType($username, $userType)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = UserFileService::substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = UserFileService::beautifyString($tmp[0], $tmp[1], $userType, $tmp[3], $tmp[4]);

            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Change tag (user setting)
     *
     * @param string $username
     * @param $tagSetting
     * @return bool returns true if tag was changed
     */
    public function changeTag($username, $tagSetting)
    {
        if (!empty($username)) {
            $f = file_get_contents("../etc/.shadow");

            $shadows = explode(PHP_EOL, $f);
            $key = UserFileService::substringInArray($username, $shadows);
            $tmp = list($username, $password, $type, $lang, $tag) = explode(":", $shadows[$key]);
            $newLine = UserFileService::beautifyString($tmp[0], $tmp[1], $tmp[2], $tmp[3], $tagSetting);

            $shadows[$key] = $newLine;
            file_put_contents("../etc/.shadow", implode(PHP_EOL, $shadows), LOCK_EX);
            return true;

        } else {
            $this->errorMessage = 'Something went wrong.';
            return false;
        }
    }

    /**
     * Returns error messages
     *
     * @return string
     */
    public function getErrors()
    {
        return $this->errorMessage;
    }
}
