<?php

namespace cj\controller;

/**
 * Class ValidationController
 *
 * Hashes strings,  compares strings and validates user data
 */
class ValidationController
{
    /**
     * @var array holds the user credentials as an associated array, according to .shadow file layout
     */
    public $credentials = array();

    /**
     * @var string
     */
    public $errorMessage = '';

    /**
     * @var string
     */
    public $loginErrorMessage = '';

    /**
     * getter for the credentials array
     *
     * @return array an associated array holding the credentials, according to
     * .shadow file layout
     */
    private function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * This method compares two given password strings and checks them for validity
     *
     * @param string $password the first password string
     * @param string $passwordRepeat the second password string
     * @return bool returns true if the passwords are equal and valid
     */
    public function comparePasswords($password, $passwordRepeat)
    {
        $pattern = "^(?=.*[a-z])(?=.*\d).{8,}^";

        //$passOne = "eee"; // uncomment to echo the error message
        if (preg_match($pattern, $password)) {
            if (strcmp($password, $passwordRepeat) == 0) {
                return true;
            } else {
                $this->errorMessage = 'Password and password repeat are not identical.';
                return false;
            }
        } else {
            $this->errorMessage = 'Password: Minimum eight characters, at least one number/letter.';
            return false;
        }
    }

    /**
     * This method creates a hash for a given password string
     *
     * @param string $password the given password string
     * @return bool|string either returns false if the password is invalid or returns the password hash
     */
    public function createHash($password)
    {
        if (strlen($password) >= 8) {
            return password_hash($password, PASSWORD_DEFAULT);
        } else {
            $this->errorMessage = 'Password must be at least 8 characters.';
            return false;
        }
    }

    /**
     * This method checks if the given password matches the hash
     *
     * @param string $password the given password
     * @param string $hash the given password hash
     * @return bool returns true if the password and the hash match
     */
    public function verifyPassword($password, $hash)
    {
        if (password_verify($password, $hash)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method loads the .shadow file into the credentials array
     *
     * @return bool returns true if the .shadow file exists, false if not.
     */
    private function loadShadowFile()
    {
        if (file_exists("../etc/.shadow")) {
            $fn = fopen("../etc/.shadow", "r") or die("fail to open file");

            while ($row = fgets($fn)) {
                list($username, $password, $type, $lang, $tag) = explode(":", $row);

                $this->credentials[] = array(
                    'username' => $username,
                    'pw'       => $password,
                    'type'     => $type,
                    'lang'     => $lang,
                    'tag'      => $tag,
                );
            }
            fclose($fn);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method validates the given username and password by
     * comparing it with the .shadow file
     *
     * @param string $name the given username to compare
     * @param string $password the given password to compare
     * @return bool returns true if the given username and password are valid
     */
    public function isUserValid($name, $password)
    {
        $this->loadShadowFile();
        $shadows = $this->getCredentials();
        $pos = null;
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $name) {
                $pos = $key;
            }
        }
        if ($this->verifyPassword($password, $shadows[$pos]['pw'])) {
            return true;
        } else {
            $this->errorMessage = 'Login incorrect.';
            return false;
        }
    }

    /**
     * Checks if username is present in .shadow file
     *
     * @param string $name
     * @return bool
     */
    public function isUserRegistered($name)
    {
        $this->loadShadowFile();
        $shadows = $this->getCredentials();
        $pos = null;
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $name) {
                return true;
            }
        }
        $this->errorMessage = 'Login incorrect.';
        return false;
    }

    /**
     * Check if user is active (tag === 1)
     *
     * @param string $name
     * @return bool
     */
    public function isUserActive($name)
    {
        $this->loadShadowFile();
        $shadows = $this->getCredentials();
        $pos = null;
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $name) {
                $pos = $key;
                if ($shadows[$pos]['tag'] == 1) {
                    return true;
                }
            }
        }
        $this->loginErrorMessage = 'Login incorrect.';
        return false;
    }

    /**
     * Returns user data and settings
     *
     * @param string $name
     * @param string $attribute (username, pw, type, lang, tag)
     * @return bool
     */
    public function userData($name, $attribute)
    {
        $this->loadShadowFile();
        $shadows = $this->getCredentials();
        $pos = null;
        if ($attribute == 'pw') {
            return false;
        }
        foreach ($shadows as $key => $value) {
            if ($value['username'] === $name) {
                $pos = $key;
                return preg_replace('/\s+/', '', $shadows[$pos][$attribute]);
            }
        }
        return false;
    }

    /**
     * Getter for the error message string
     *
     * @return string returns the error message
     */
    public function getErrors()
    {
        return $this->errorMessage;
    }

    /**
     * Getter for login error message
     *
     * @return string returns the error message
     */
    public function getLoginErrors()
    {
        return $this->loginErrorMessage;
    }

}
