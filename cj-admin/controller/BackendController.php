<?php

namespace cj\controller;

/**
 * Class BackendController
 *
 * This class handles the dynamic page generation for the backend interface.
 */
require_once('BaseTemplateController.php');
require_once('../service/JsonService.php');
require_once('../service/FileHandler.php');
require_once ('../service/UserFileService.php');

use cj\service\JsonService;
use cj\service\FileHandler;
use cj\service\UserFileService;
use cj\model\View;

class BackendController extends BaseTemplateController
{
    private $innerView = null;
    private $outerView = null;
    private $fileHandler = null;
    private $language = null;
    private $customCss = null;
    private $config = null;

    /**
     * BackendController constructor
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->outerView = new View();
        $this->fileHandler = new FileHandler();
        // if no specific template is given in the request as view, hand out the default template
        $request['view'] = !empty($request['view']) ? $request['view'] : 'home';
        $this->language = isset($request['lang']) ? $request['lang'] : JsonService::getDefaultLanguage();
        $this->config = include('../config/config.php');
        parent::__construct($request);
    }

    /**
     * {@inheritdoc}
     *
     * This method checks for a given template to display and loads it into the backend.outer template
     *
     * @return mixed the template of the outer view
     */
    protected function display()
    {
        $this->innerView = new View();
        switch ($this->template) {
            case 'contenteditor':
                $this->innerView->setTemplate('backend.inner.contenteditor');
                $this->customCss = '../../' . $this->config['cssPath'] . 'contenteditor-style.css';
                $site = !empty($this->request['site']) ? $this->request['site'] : 'index';

                $this->innerView->assign('site', $site);
                $this->innerView->assign('content', JsonService::loadContentJsonFile($site, $this->language));
                $this->innerView->assign('languages', JsonService::loadLanguages());
                break;
            case 'languageeditor':
                $this->innerView->setTemplate('backend.inner.languageeditor');
                $this->customCss = '../../' . $this->config['cssPath'] . 'languageeditor-style.css';
                $this->innerView->assign('languages', JsonService::loadLanguages());
                break;
            case 'posteditor':
                $this->innerView->setTemplate('backend.inner.posteditor');
                $this->customCss = '../../' . $this->config['cssPath'] . 'posteditor-style.css';

                $site = !empty($this->request['site']) ? $this->request['site'] : 'index';
                $this->innerView->assign('site', $site);
                $this->innerView->assign('post', JsonService::loadPostJsonFile($site, $this->language));
                $this->innerView->assign('languages', JsonService::loadLanguages());
                break;
            case 'users':
                $this->innerView->setTemplate('backend.inner.users');
                $this->innerView->assign('users', UserFileService::getUsers());
                $this->customCss = '../../' . $this->config['cssPath'] . 'users-style.css';
                break;
            case 'settings':
                $this->innerView->setTemplate('backend.inner.settings');
                $this->customCss = '../../' . $this->config['cssPath'] . 'settings-style.css';
                break;
            case 'home':
                $this->innerView->setTemplate('backend.inner.home');
                $this->customCss = '../..' . $this->config['cssPath'] . 'home-style.css';
                break;
            default:
                $this->innerView->setTemplate('backend.inner.home');
        }
        $this->outerView->setTemplate('backend.outer');
        $this->outerView->assign('backend_content', $this->innerView->loadTemplate());
        $this->outerView->assign('sites', $this->fileHandler->getHtmlFiles());
        $this->outerView->assign('backend_custom_css', $this->customCss);

        return $this->outerView->loadTemplate();
    }

    /**
     * {@inheritdoc}
     */
    public function handleGetOrPost()
    {
        switch ($this->template) {
            case 'contenteditor':
                // save modified content if any exists
                if (isset($this->request['modified_content']) && isset($this->request['modified_content_parents'])) {
                    // take the first site you can get if no site is given
                    if (!isset($this->request['site'])) {
                        $handler = new FileHandler;
                        $this->request['site'] = $handler->getHtmlFiles()[0];
                    }
                    JsonService::updateContentJsonFile(JsonService::formatArraysForContentJson(
                        $this->request['modified_content'],
                      $this->request['modified_content_parents']
                    ), $this->request['site'], $this->language);
                }
                break;
            case 'languageeditor':
                JsonService::updateLanguageJsonFile($_POST);
                break;
            case 'posteditor':
                if (isset($this->request['modified_headline']) && isset($this->request['modified_post']) && isset($this->request['modified_date'])) {
                    if (!isset($this->request['site'])) {
                        $handler = new FileHandler();
                        $this->request['site'] = $handler->getHtmlFiles()[0];
                    }
                    JsonService::updatePostJsonFile(
                        JsonService::formatArraysForPostJson(
                        $this->request['modified_headline'],
                      $this->request['modified_post'],
                        $this->request['modified_date']
                    ),
                        $this->request['site'],
                      $this->language
                    );
                }
                break;
            case 'home':
            default:
                //nothing to do yet
        }
        echo $this->display();
    }
}
