<div id="helpModal" class="helpModal">
    <!-- Modal content -->
    <div class="modal-content help">
        <span class="close"
              id="closeHelpModal">&times;</span>
        <?php
        if ($_GET['view'] == 'home' || !isset($_GET['view'])) {
            if ($_SESSION['type'] == 1) {
                echo $translation['help.dashboard.admin'];
            }
            if ($_SESSION['type'] == 0) {
                echo  $translation['help.dashboard.chiefEditor'];
            }
            if ($_SESSION['type'] == 2) {
                echo $translation['help.dashboard.editor'];
            }
        }
        if ($_GET['view'] == 'contenteditor') {
            echo $translation['help.contenteditor'];
        }
        if ($_GET['view'] == 'posteditor') {
            echo $translation['help.posteditor'];
        }
        if ($_GET['view'] == 'languageeditor') {
            echo $translation['help.languageeditor'];
        }
        if ($_GET['view'] == 'users') {
            echo $translation['help.users'];
        }
        if ($_GET['view'] == 'settings') {
            if ($_SESSION['type'] == 1) {
                echo $translation['help.settings.admin'];
            } else {
                echo $translation['help.settings'];
            }
        }
        ?>
    </div>
</div>
