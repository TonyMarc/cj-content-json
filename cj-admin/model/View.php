<?php
namespace cj\model;

/**
 * This Class takes data from a controller and fills a template with the given data
 */
class View
{

    /**
     * @var string path to the template
     */
    private $path = '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;

    /**
     * @var string holds the name of the template. It is initialized as the default template name
     */
    private $template = 'home';

    /**
     * @var array an associative array that holds the variables that can be accessed from the template
     */
    private $_ = array();

    /**
     * This method assigns a value to a key in the $_ array
     *
     * @param String $key the key
     * @param String|array $value the variable to assign
     */
    public function assign($key, $value)
    {
        $this->_[$key] = $value;
    }


    /**
     * This method sets the name of the template to use
     *
     * @param String $template the name of the template
     */
    public function setTemplate($template = 'home')
    {
        $this->template = $template;
    }

    /**
     * This method loads and returns the template file
     *
     * @return string the content of the template file
     */
    public function loadTemplate()
    {
        // the path to the file
        $file = $this->path . $this->template . '.php';
        // check if the file exists

        if (file_exists($file)) {
            // start the output buffer
            ob_start();
            // include the template file
            include $file;
            // save the content of the buffer into the output variable
            $output = ob_get_contents();
            // clean and remove the buffer
            ob_end_clean();

            return $output;
        } else {
            // the template file does not exist. Thus we return an error message
            return 'could not load template ' . $this->template;
        }
    }
}
