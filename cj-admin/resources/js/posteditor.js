function insertFormFields(argument) {
    var node = argument;
    node.parentNode.parentNode.insertAdjacentHTML('beforebegin',
        '<div class="postGroup"><div class="card"><img src="../resources/img/delete.svg" id="deleteButton" title="Delete post" class="remove" onclick="showDeleteModal(this)"> <img src="../resources/img/addButton.svg" id="addButton" class="add" title="Add post" onclick="showAddModal(this)"><div class="cardMain"><label class="headline"><h3>headline</h3></label></div><div class="cardFooterPosts"><textarea rows="2" placeholder="Insert text" name="modified_headline[]"></textarea></div><div class="cardMain"> <label><h3>post</h3></label></div><div class="cardFooterPosts"><textarea rows="5" placeholder="Insert text" name="modified_post[]"></textarea></div><div class="cardMain"><label><h3>date</h3></label></div><div class="cardFooter"><textarea rows="2" placeholder="Insert text" name="modified_date[]"></textarea></div></div><hr></div>');
}

function removeFormFields(argument) {
    var node = argument;
    node.parentNode.parentNode.parentNode.removeChild(node.parentNode.parentNode);
}

function showAddModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalAddDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeAddDialog');

    var buttonNo = document.getElementById("addNo");
    var buttonYes = document.getElementById("addYes");


    // Closes dialog box
    span.onclick = function() {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Removes form fields
    buttonYes.onclick = function () {
        insertFormFields(argument);
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

function showDeleteModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalDeleteDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeDeleteDialog');

    var buttonNo = document.getElementById("deleteNo");
    var buttonYes = document.getElementById("deleteYes");


    // Closes dialog box
    span.onclick = function() {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Removes form fields
    buttonYes.onclick = function () {
        removeFormFields(argument);
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

