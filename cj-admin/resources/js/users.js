/**
 * Displays change password form
 *
 * @param argument
 */
function showChangePasswordModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalChangePasswordDialog');
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementById('closeChangePasswordDialog');
    var headline = document.getElementById('headlineChangePasswordDialog');
    var buttonChangePassword = document.getElementById('changePassword');

    buttonChangePassword.value = argument.value;

    var headlineBackup = headline.innerHTML;
    headline.innerHTML += argument.value + '?';

    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
        headline.innerHTML = headlineBackup;
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            headline.innerHTML = headlineBackup;
        }
    };
}

/**
 * Displays user deletion form
 *
 * @param argument
 */
function showDeleteModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalDeleteDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeDeleteDialog');

    var buttonNo = document.getElementById("deleteNo");
    var buttonYes = document.getElementById("deletion-login");

    buttonYes.value = argument.value;

    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

/**
 * Displays registration link delete form
 *
 * @param argument
 */
function showRegistrationLinkDeleteModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalDeleteRegistrationLink');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeDeleteRegistrationLink');

    var buttonNo = document.getElementById("deleteRegistrationLinkNo");
    var buttonYes = document.getElementById("deleteRegistrationLink");

    buttonYes.value = argument.value;

    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

/**
 * Displays registration link copy confirmation
 *
 * @param argument
 */
function showCopyConfirmationModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalCopyConfirmation');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeCopyConfirmation');

    var buttonYes = document.getElementById('copyConfirmationOK');
    var input = document.getElementById('copyConfirmationInput');

    input.value = argument.value;
    input.select();

    try {
        document.execCommand("copy");
    } catch (e) {

    }


    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonYes.onclick = function () {
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

/**
 * Displays user activation form
 *
 * @param argument
 */
function showUserActivationModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalActivateUser');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeActivateUser');

    var headline = document.getElementById('headlineActivateUserDialog');
    var headlineBackup = headline.innerHTML;
    headline.innerHTML += argument.value + '?';

    var buttonNo = document.getElementById('activateNo');
    var buttonYes = document.getElementById('activateUser');

    buttonYes.value = argument.value;


    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
        headline.innerHTML = headlineBackup;
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
        headline.innerHTML = headlineBackup;
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            headline.innerHTML = headlineBackup;
        }
    };
}

/**
 * Displays change user role form
 *
 * @param argument
 */
function showChangeUserRoleModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalChangeUserRole');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeChangeUserRole');

    var headline = document.getElementById('headlineChangeUserRoleDialog');
    var headlineBackup = headline.innerHTML;
    headline.innerHTML += argument.value + '?';

    var buttonYes = document.getElementById('changeUserRoleYes');

    buttonYes.value = argument.value;


    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
        headline.innerHTML = headlineBackup;
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            headline.innerHTML = headlineBackup;
        }
    };
}
