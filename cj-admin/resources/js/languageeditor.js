/**
 * The methods in this javascript file are used used in the language editor template and offer every logic needed.
 */

/**
 * This method can be used to insert an element after another element
 *
 * @param el the element to be inserted
 * @param referenceNode the reference element to insert afterwards
 */
function insertAfter(el, referenceNode)
{
    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
}

/**
 * This method clones the last fieldset of the language editor, fills every element with the languages contained
 * in the passed button/form and inserts the clone after the last fieldset.
 *
 * @param button the add language button containing the info from the form
 */
function appendLanguageFieldset(button)
{
    // TODO check if language already exists
    //get input fields
    var langShortInput = button.previousElementSibling;
    var langLongInput = langShortInput.previousElementSibling;

    if (!validateLanguageInput(langLongInput, langShortInput)) {
        return;
    }

    // get values
    var langShort = langShortInput.value.toLowerCase();
    var langLong = langLongInput.value;

    // reset inputs
    langShortInput.value = "";
    langLongInput.value = "";

    //get the last fieldset
    var lastLanguageFieldset = document.body.querySelector("fieldset:last-of-type");
    var fieldsetClone = lastLanguageFieldset.cloneNode(true);

    // get all elements inside the fieldset
    var legend = fieldsetClone.firstChild.nextSibling;
    legend.innerHTML = langLong + " (" + langShort + ")";
    var legendInputLong = legend.nextElementSibling;
    legendInputLong.value = langLong;
    var legendInputShort = legendInputLong.nextElementSibling;
    legendInputShort.value = langShort;
    var activeCheckbox = legendInputShort.nextElementSibling;
    activeCheckbox.value = langShort;
    activeCheckbox.checked = true;
    addUncheckListener(activeCheckbox, "delete");
    var defaultRadio = activeCheckbox.nextElementSibling;
    defaultRadio.value = langShort;
    var deleteCheckbox = defaultRadio.nextElementSibling;
    deleteCheckbox.value = langShort;
    deleteCheckbox.checked = false;
    addUncheckListener(deleteCheckbox, "active");

    // add the new fieldset after the last existing fieldset
    insertAfter(fieldsetClone, lastLanguageFieldset);

    // check if the clone radio is selected. If true, set the previous language to default.
    if (defaultRadio.checked) {
        var lastDefaultRadio = getSiblingByClass(lastLanguageFieldset.firstChild.nextSibling, "default");
        lastDefaultRadio.checked = true;
    }

    //create a new hidden input to mark this element as new
    var inputNew = document.createElement("INPUT");
    inputNew.setAttribute("type", "hidden");
    inputNew.setAttribute("value", langShort);
    inputNew.setAttribute("name", "new[]");
    insertAfter(inputNew, deleteCheckbox);
}

/**
 * This method checks the passed language inputs for validity and report to the user if the input is invalid
 *
 * @param long the input containing the language's long name
 * @param short the input containing the language's abbreviation
 * @returns {boolean} true if the input is valid
 */
function validateLanguageInput(long, short)
{
    long.setCustomValidity(long.title);
    short.setCustomValidity(short.title);
    if (long.value.length < 1) {
        //TODO
        long.reportValidity();
        return false;
    } else {
        if (short.value.length !== 2) {
            short.reportValidity();
            return false;
        }
    }
    return true;
}

/**
 * This method takes an element and searches all its siblings for a given class. If an element with the given class is
 * found, it is returned.
 *
 * @param element the element to check the siblings for
 * @param siblingClass the class to check for
 * @returns an element with the given class or null if no element was found
 */
function getSiblingByClass(element, siblingClass)
{
    var siblings = element.parentNode.childNodes;
    var result = null;
    siblings.forEach(function (value) {
        if (value != null && value.nodeType !== Node.TEXT_NODE) {
            if (value.className === siblingClass) {
                result = value;
            }
        }
    });
    return result;
}

/**
 * This method adds EventListeners on every element of the given class to uncheck the sibling element with the other
 * class given, when itself is checked.
 *
 * @param checkboxClassWithListener the clss to add the EventListener to
 * @param checkboxClassToDisable the class to uncheck
 */
function initUncheckListeners(checkboxClassWithListener, checkboxClassToDisable)
{
    var checkboxToAddListener = document.body.getElementsByClassName(checkboxClassWithListener);
    for (var i = 0, len = checkboxToAddListener.length; i < len; i++) {
        addUncheckListener(checkboxToAddListener[i], checkboxClassToDisable);
    }
}

/**
 * This method adds an EventListener to an element that unchecks a sibling with a specific class when checked.
 *
 * @param elementWithListener the element to add the listener to
 * @param elementClassToDisable the class to uncheck
 */
function addUncheckListener(elementWithListener, elementClassToDisable)
{
    elementWithListener.addEventListener('click', function () {
        var defaultRadio = getSiblingByClass(this, "default");
        if (defaultRadio.checked) {
            consumeEvent();
            return;
        }

        // uncheck the checkbox to disable if this is checked
        if (this.checked === true) {
            var uncheckCheckbox = getSiblingByClass(this, elementClassToDisable);
            if (uncheckCheckbox.checked === true) {
                uncheckCheckbox.checked = false;
            }
        }
    });
}

/**
 * This method takes the radio button of the language editor, checks the adjacent active checkbox and unchecks the
 * adjacent delete checkbox
 *
 * @param radio the radio
 */
function defaultSetActiveAndDelete(radio)
{
    var activeCheckbox = radio.previousElementSibling;
    var deleteCheckbox = radio.nextElementSibling;

    activeCheckbox.checked = true;
    deleteCheckbox.checked = false;
}

/**
 * This method consumes an event and stops its propagation
 */
function consumeEvent()
{
    event.preventDefault();
    event.stopPropagation();
}

// add a listener to each active checkbox to watch the delete checkboxes
initUncheckListeners("active", "delete");
// add a listener to each delete checkbox to watch the active checkboxes
initUncheckListeners("delete", "active");


