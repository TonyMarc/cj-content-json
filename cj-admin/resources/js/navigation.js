/**
 * Toggles menu in responsive mode (min-width: 700px)
 */
function menu() {
    var aside = document.getElementById("asideNav");
    var nav = document.getElementById("navNav");
    var main = document.getElementById("main");
    var menuButton = document.getElementById("menuButton");

    // Try to determine the viewport width
    try {
        width = window.innerWidth;
    } catch (e) {
        var width = 700;
    }

    try {
        var h2 = main.querySelector('h2');
        if (h2.className === "") {
            h2.className += "cardOpenMenu";
            h2.style.width = width + "px";
        } else {
            h2.className = "";
            h2.getAttribute('style');
            h2.removeAttribute('style');
        }
    } catch (e) {

    }

    try {
        var languageForm = document.querySelector('.languages-form');
        var addLanguagesForm = document.querySelector('.add-language-form');

        if (languageForm.className === "languages-form") {
            languageForm.className += " cardOpenMenu";
            languageForm.style.width = width + "px";
        } else {
            languageForm.className = "languages-form";
            languageForm.getAttribute('style');
            languageForm.removeAttribute('style');
        }

        if (addLanguagesForm.className === "add-language-form") {
            addLanguagesForm.className += " cardOpenMenu";
            addLanguagesForm.style.width = width + "px";
        } else {
            addLanguagesForm.className = "add-language-form";
            addLanguagesForm.getAttribute('style');
            addLanguagesForm.removeAttribute('style');
        }

    } catch (e) {

    }

    try {
        var userTable = document.querySelector('.userTable');

        if (userTable.className === "userTable") {
            userTable.className += " tableOpenMenu";
            userTable.style.width = width + "px";
        } else {
            userTable.className = "userTable";
            userTable.getAttribute('style');
            userTable.removeAttribute('style');
        }
    } catch (e) {

    }

    try {
        var cards = document.getElementsByClassName('card');

        var i;
        for (i = 0; i < cards.length; i++) {
            if (cards[i].className === "card") {
                cards[i].className += " cardOpenMenu";
                cards[i].style.width = width + "px";
            } else {
                cards[i].className = "card";
                cards[i].getAttribute('style');
                cards[i].removeAttribute('style');
            }
        }
    } catch (e) {

    }

    // Get user menu
    var userMenu = document.getElementById("userMenu");
    var overlay = document.getElementById("overlay");

    if (aside.className === "aside") {
        aside.className += " responsive";
        nav.className += " responsive";
        main.className += " responsive";
        menuButton.className += " responsive";
        // Check if user menu is open
        if (userMenu.className !== "userMenu") {
            overlay.style.display = "none";
            userMenu.className = "userMenu";
        }
    } else {
        aside.className = "aside";
        nav.className = "nav";
        main.className = "content";
        menuButton.className = "icon";
    }
}

/**
 * Toggles user-menu
 */
function userMenu() {
    var userMenu = document.getElementById("userMenu");
    var overlay = document.getElementById("overlay");

    // Get the main menu
    var aside = document.getElementById("asideNav");
    var nav = document.getElementById("navNav");
    var main = document.getElementById("main");
    var menuButton = document.getElementById("menuButton");


    if (userMenu.className === "userMenu") {
        overlay.style.display = "block";
        userMenu.className += " responsive";
        // Check if main menu is open
        if (aside.className !== "aside") {
            aside.className = "aside";
            nav.className = "nav";
            main.className = "content";
            menuButton.className = "icon";
        }
    } else {
        overlay.style.display = "none";
        userMenu.className = "userMenu";
    }
    // Closes overlay if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == overlay) {
            overlay.style.display = "none";
            userMenu.className = "userMenu";
        }
    };
}
